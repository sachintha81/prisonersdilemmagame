﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PrisonersDilemma.StrategyCommon;

namespace PrisonersDilemma.Model
{
    public class GameResult
    {
        private StrategyInfo player1Info;

        private StrategyInfo player2Info;

        private int iterations;

        private int player1Points;

        private int player2Points;

        private List<HistoryItem> history;

        public int Player1Points
        {
            get
            {
                return player1Points;
            }
            set
            {
                player1Points = value;
            }
        }

        public int Player2Points
        {
            get
            {
                return player2Points;
            }
            set
            {
                player2Points = value;
            }
        }

        public int Iterations
        {
            get
            {
                return iterations;
            }
            set
            {
                iterations = value;
            }
        }

        public StrategyInfo Player1Info
        {
            get
            {
                return player1Info;
            }

            set
            {
                player1Info = value;
            }
        }

        public StrategyInfo Player2Info
        {
            get
            {
                return player2Info;
            }

            set
            {
                player2Info = value;
            }
        }

        public List<HistoryItem> History
        {
            get
            {
                return history;
            }

            set
            {
                history = value;
            }
        }

        public GameResult()
        {
            player1Info = new StrategyInfo();
            player2Info = new StrategyInfo();
            player1Points = 0;
            player2Points = 0;
            iterations = 0;
            history = new List<HistoryItem>();
        }
        
        public void AddHistoryItem(HistoryItem item)
        {
            history.Add(item);
        }
    }
}
