﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrisonersDilemma.Model
{
    public class GameStats
    {
        private int total;
        private double average;

        private int numC;
        private int totC;
        private double avgC;

        private int numD;
        private int totD;
        private double avgD;

        public GameStats()
        {

        }

        public GameStats(int tot, double avg, int c, int cTot, double cAvg, int d, int dTot, int dAvg)
        {
            total = tot;
            average = avg;

            numC = c;
            totC = cTot;
            avgC = cAvg;

            numD = d;
            totD = dTot;
            avgD = dAvg;
        }

        public int Total
        {
            get
            {
                return total;
            }

            set
            {
                total = value;
            }
        }

        public double Average
        {
            get
            {
                return average;
            }

            set
            {
                average = value;
            }
        }

        public int NumC
        {
            get
            {
                return numC;
            }

            set
            {
                numC = value;
            }
        }

        public double AvgC
        {
            get
            {
                return avgC;
            }

            set
            {
                avgC = value;
            }
        }

        public int NumD
        {
            get
            {
                return numD;
            }

            set
            {
                numD = value;
            }
        }

        public double AvgD
        {
            get
            {
                return avgD;
            }

            set
            {
                avgD = value;
            }
        }

        public int TotC
        {
            get
            {
                return totC;
            }

            set
            {
                totC = value;
            }
        }

        public int TotD
        {
            get
            {
                return totD;
            }

            set
            {
                totD = value;
            }
        }
    }
}
