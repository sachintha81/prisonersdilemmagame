﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PrisonersDilemma.StrategyCommon;

namespace PrisonersDilemma.Model
{
    public class Strategy
    {
        public Strategy(StrategyInfo info, object instance, Type type)
        {
            stratInfo = new StrategyInfo();
            stratInfo.StrategyName = info.StrategyName;
            stratInfo.StrategyType = info.StrategyType;
            stratInfo.Description = info.Description;
            stratInfo.HasOptions = info.HasOptions;
            instanceObj = instance;
            typeObj = type;
        }

        private StrategyInfo stratInfo;

        private object instanceObj;

        private Type typeObj;

        public StrategyInfo StratInfo
        {
            get { return stratInfo; }
            set { stratInfo = value; }
        }

        public object InstanceObj
        {
            get { return instanceObj; }
            set { instanceObj = value; }
        }

        public Type TypeObj
        {
            get { return typeObj; }
            set { typeObj = value; }
        }
    }
}
