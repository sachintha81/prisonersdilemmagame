﻿namespace PrisonersDilemma.Common
{
    public static class Common
    {
        public const string StrategyNameProperty = "StrategyName";
        public const string StrategyTypeProperty = "StrategyType";
        public const string StrategyDescriptionProperty = "Description";
        public const string StrategyHasOptionsProperty = "HasOptions";

        public const string Method_Play = "PlayCard";
        public const string Method_Flush = "Flush";
        public const string Method_SetPoints = "SetPointScheme";
        public const string Method_GetOptions = "GetOptions";
        public const string Method_SetOptions = "SetOptions";
        public const string Method_GetStrategyInfo = "GetStrategyInfo";

        public const string SettingReward = "Reward";
        public const string SettingPunishment = "Punishment";
        public const string SettingPayoff = "Payoff";
        public const string SettingTemptation = "Temptation";
    }
}
