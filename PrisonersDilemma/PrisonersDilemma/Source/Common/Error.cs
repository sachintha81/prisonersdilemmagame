﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrisonersDilemma.Common
{
    public static class Error
    {
        public static string SUCCESS = "SUCCESS!";
        public static string ERROR = "ERROR!";
        public static string WARNING = "Warning!";
        public static string INFO = "Info";
        public static string PLAY_CARD_FAILED = "Failed to play a card.";
    }
}
