﻿using System;
using System.Collections.Generic;
using System.Reflection;
// PrisonersDilemma
using PrisonersDilemma.Common;
using PrisonersDilemma.Model;
using PrisonersDilemma.StrategyCommon;
using static PrisonersDilemma.Common.Common;
using static PrisonersDilemma.StrategyCommon.CommonTypes;

namespace PrisonersDilemma
{
    public static class Gameplay
    {
        /// <summary>
        /// Play one strategy against another.
        /// </summary>
        /// <param name="pointScheme">Point scheme to play the game.</param>
        /// <param name="s1">Strategy 1</param>
        /// <param name="s2">Strategy 2</param>
        /// <param name="iterations"># Iterations</param>
        /// <param name="results">Game results</param>
        /// <param name="exception">Execption, if any.</param>
        /// <returns>Success/Failure : True/False</returns>
        public static bool PlayOneVsOne(PointScheme pointScheme, Strategy s1, Strategy s2, int iterations, out GameResult results, out Exception exception)
        {
            bool ret = true;
            exception = new Exception(Error.SUCCESS);
            results = new GameResult();

            try
            {
                Type s1TypeObj = s1.TypeObj;
                Type s2TypeObj = s2.TypeObj;

                object inst1 = s1.InstanceObj;
                object inst2 = s2.InstanceObj;

                MethodInfo setPointsMethod1 = s1TypeObj.GetMethod(Method_SetPoints);
                MethodInfo setPointsMethod2 = s2TypeObj.GetMethod(Method_SetPoints);

                MethodInfo playMethod1 = s1TypeObj.GetMethod(Method_Play);
                MethodInfo playMethod2 = s2TypeObj.GetMethod(Method_Play);

                int totalPoints1 = 0;
                int totalPoints2 = 0;
                int roundPoints1 = 0;
                int roundPoints2 = 0;

                Card card1 = Card.None;
                Card card2 = Card.None;
                List<Card> movesHistoryP1 = new List<Card>();
                List<Card> movesHistoryP2 = new List<Card>();
                List<int> pointsHistoryP1 = new List<int>();
                List<int> pointsHistoryP2 = new List<int>();
                object ret1 = null;
                object ret2 = null;

                // Play the game 'iterations' number of times.
                for (int i = 1; i <= iterations; i++)
                {
                    object[] params1 = new object[6] { i, movesHistoryP1, movesHistoryP2, pointsHistoryP1, pointsHistoryP2, null };
                    object[] params2 = new object[6] { i, movesHistoryP2, movesHistoryP1, pointsHistoryP2, pointsHistoryP1, null };

                    ret1 = playMethod1.Invoke(inst1, params1);
                    ret2 = playMethod2.Invoke(inst2, params2);

                    if (!(bool)ret1 || !(bool)ret2)
                    {
                        exception = new Exception(Error.PLAY_CARD_FAILED);
                        return false;
                    }

                    card1 = (Card)params1[5];
                    card2 = (Card)params2[5];

                    // Decide each player's points according to the point scheme annd cards played.
                    ScorePoints(pointScheme, card1, card2, out roundPoints1, out roundPoints2);

                    // Keep track of moves and points scored history.
                    movesHistoryP1.Add(card1);
                    movesHistoryP2.Add(card2);
                    pointsHistoryP1.Add(roundPoints1);
                    pointsHistoryP2.Add(roundPoints2);
                    totalPoints1 += roundPoints1;
                    totalPoints2 += roundPoints2;
                    results.AddHistoryItem(new HistoryItem(i, card1, card2, roundPoints1, roundPoints2));
                }

                StrategyInfo p1Info = new StrategyInfo(s1.StratInfo.StrategyName, s1.StratInfo.StrategyType, s1.StratInfo.Description, s1.StratInfo.HasOptions);
                StrategyInfo p2Info = new StrategyInfo(s2.StratInfo.StrategyName, s2.StratInfo.StrategyType, s2.StratInfo.Description, s2.StratInfo.HasOptions);
                results.Player1Info = p1Info;
                results.Player2Info = p2Info;
                results.Player1Points = totalPoints1;
                results.Player2Points = totalPoints2;
                results.Iterations = iterations;

                MethodInfo flushMethod1 = s1TypeObj.GetMethod(Method_Flush);
                MethodInfo flushMethod2 = s2TypeObj.GetMethod(Method_Flush);
                flushMethod1.Invoke(inst1, null);
                flushMethod2.Invoke(inst2, null);
            }
            catch (Exception ex)
            {
                exception = ex;
                ret = false;
            }

            return ret;
        }

        /// <summary>
        /// Play the game, one strategy against multiple other strategies.
        /// </summary>
        /// <param name="s1">Selected strategy</param>
        /// <param name="sList">List of strategies to play against.</param>
        /// <param name="pointScheme">Point scheme</param>
        /// <param name="iterations"># iterations</param>
        /// <param name="resultList">Results</param>
        /// <param name="exception">Exception, if any.</param>
        /// <returns></returns>
        public static bool PlayOneVsMany(
                                            Strategy s1,
                                            List<Strategy> sList,
                                            PointScheme pointScheme,
                                            int iterations,
                                            out List<GameResult> resultList,
                                            out Exception exception)
        {
            bool ret = true;
            exception = new Exception(Error.SUCCESS);
            resultList = null;

            try
            {
                resultList = new List<GameResult>(sList.Count);
                foreach (Strategy st in sList)
                {
                    GameResult result;
                    if (!PlayOneVsOne(pointScheme, s1, st, iterations, out result, out exception))
                    {
                        ret = false;
                        break;
                    }
                    else {
                        resultList.Add(result);
                    }
                }
            }
            catch (Exception ex)
            {
                exception = ex;
                ret = false;
            }

            return ret;
        }

        /// <summary>
        /// Play multiple strategies against each other.
        /// </summary>
        /// <param name="sList">List of strategies.</param>
        /// <param name="pointScheme">Point scheme</param>
        /// <param name="iterations"># iterations</param>
        /// <param name="resultList">Results</param>
        /// <param name="exception">Exception, if any.</param>
        /// <returns></returns>
        public static bool PlayManyVsMany(List<Strategy> sList, PointScheme pointScheme, int iterations, out List<GameResult> resultList, out Exception exception)
        {
            bool ret = true;
            exception = new Exception(Error.SUCCESS);
            resultList = null;

            try
            {
                int noOfGames = (sList.Count * (sList.Count - 1)) / 2;
                resultList = new List<GameResult>(noOfGames);

                List<GameResult> oneRoundResults = null;
                foreach (Strategy strat in sList)
                {
                    if (!PlayOneVsMany(strat, sList, pointScheme, iterations, out oneRoundResults, out exception))
                    {
                        ret = false;
                        break;
                    }
                    else {
                        foreach (GameResult result in oneRoundResults)
                        {
                            resultList.Add(result);
                        }
                        oneRoundResults = null;
                    }
                }
            }
            catch (Exception ex)
            {
                exception = ex;
                ret = false;
            }

            return ret;
        }

        /// <summary>
        /// Scoring points based on the cards played and point scheme.
        /// </summary>
        /// <param name="pointScheme">Point scheme</param>
        /// <param name="card1">Card played by strategy 1</param>
        /// <param name="card2">Card played by strategy 2</param>
        /// <param name="points1">Points scored by strategy 1</param>
        /// <param name="points2">Points scored by strategy 2</param>
        public static void ScorePoints(PointScheme pointScheme, Card card1, Card card2, out int points1, out int points2)
        {
            points1 = 0;
            points2 = 0;

            if (card1 == Card.Cooperate && card2 == Card.Cooperate)
            {
                points1 = pointScheme.Reward;
                points2 = pointScheme.Reward;
            }
            else if (card1 == Card.Defect && card2 == Card.Defect)
            {
                points1 = pointScheme.Punishment;
                points2 = pointScheme.Punishment;
            }
            else if (card1 == Card.Cooperate && card2 == Card.Defect)
            {
                points1 = pointScheme.Payoff;
                points2 = pointScheme.Temptation;
            }
            else if (card1 == Card.Defect && card2 == Card.Cooperate)
            {
                points1 = pointScheme.Temptation;
                points2 = pointScheme.Payoff;
            }
        }

        /// <summary>
        /// Prints results in a specific format.
        /// *** This will be changed to print a proper report later. ***
        /// </summary>
        /// <param name="result"></param>
        /// <param name="stats1"></param>
        /// <param name="stats2"></param>
        /// <returns></returns>
        public static string PrintGame(GameResult result, GameStats stats1, GameStats stats2)
        {
            string resultStr = string.Empty;
            resultStr += result.Player1Info.StrategyName + " vs " + result.Player2Info.StrategyName;
            resultStr += Environment.NewLine;
            resultStr += "----------------------------";
            resultStr += Environment.NewLine;
            resultStr += "Iterations : " + result.Iterations.ToString();
            resultStr += Environment.NewLine;
            resultStr += "Steps:";
            resultStr += Environment.NewLine;
            resultStr += string.Format("{0, -5}\t{1, -8}\t-\t{2, -8}", "Turn", result.Player1Info.StrategyName, result.Player2Info.StrategyName);
            resultStr += Environment.NewLine;
            resultStr += "----------------------------";
            resultStr += Environment.NewLine;
            for (int i = 0; i < result.Iterations; i++)
            {
                resultStr += string.Format("{0, -5}\t{1, -8}\t-\t{2, -8}", i + 1, result.History[i].Card1, result.History[i].Card2);
                resultStr += Environment.NewLine;
            }
            resultStr += Environment.NewLine;
            resultStr += Environment.NewLine;

            resultStr += result.Player1Info.StrategyName + " Statistics";
            resultStr += Environment.NewLine;
            resultStr += "----------------------------";
            resultStr += Environment.NewLine;
            resultStr += "Total Points = " + stats1.Total;
            resultStr += Environment.NewLine;
            resultStr += "Average = " + stats1.Average;
            resultStr += Environment.NewLine;
            resultStr += "Number of C = " + stats1.NumC;
            resultStr += Environment.NewLine;
            resultStr += "Points for C = " + stats1.TotC;
            resultStr += Environment.NewLine;
            resultStr += "Average for C = " + stats1.AvgC;
            resultStr += Environment.NewLine;
            resultStr += "Number of D = " + stats1.NumD;
            resultStr += Environment.NewLine;
            resultStr += "Points for D = " + stats1.TotD;
            resultStr += Environment.NewLine;
            resultStr += "Average for D = " + stats1.AvgD;
            resultStr += Environment.NewLine;
            resultStr += Environment.NewLine;

            resultStr += result.Player2Info.StrategyName + " Statistics";
            resultStr += Environment.NewLine;
            resultStr += "----------------------------";
            resultStr += Environment.NewLine;
            resultStr += "Total Points = " + stats2.Total;
            resultStr += Environment.NewLine;
            resultStr += "Average = " + stats2.Average;
            resultStr += Environment.NewLine;
            resultStr += "Number of C = " + stats2.NumC;
            resultStr += Environment.NewLine;
            resultStr += "Points for C = " + stats2.TotC;
            resultStr += Environment.NewLine;
            resultStr += "Average for C = " + stats2.AvgC;
            resultStr += Environment.NewLine;
            resultStr += "Number of D = " + stats2.NumD;
            resultStr += Environment.NewLine;
            resultStr += "Points for D = " + stats2.TotD;
            resultStr += Environment.NewLine;
            resultStr += "Average for D = " + stats2.AvgD;
            resultStr += Environment.NewLine;
            resultStr += Environment.NewLine;

            return resultStr;
        }
    }
}