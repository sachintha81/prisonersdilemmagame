﻿using PrisonersDilemma.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace PrisonersDilemma.LocalServices
{
    /// <summary>
    /// Common dialog service class for opening and closing views.
    /// </summary>
    public class DialogService
    {
        Window viewStrategyOptions = null;

        Window viewGlobalSettings = null;

        Window viewGameplay = null;

        Window viewAdvancedStats = null;

        public void ShowOptionsDialog()
        {
            viewStrategyOptions = new ViewStrategyOptions();
            viewStrategyOptions.ShowDialog();
        }

        public void CloseOptionsDialog()
        {
            if (viewStrategyOptions != null)
                viewStrategyOptions.Close();
        }

        public void ShowSettingsDialog()
        {
            viewGlobalSettings = new ViewGlobalSettings();
            viewGlobalSettings.ShowDialog();
        }

        public void CloseSettingsDialog()
        {
            if (viewGlobalSettings != null)
                viewGlobalSettings.Close();
        }

        public void ShowGameplayyDialog()
        {
            viewGameplay = new ViewGameplay();
            viewGameplay.ShowDialog();
        }

        public void CloseGameplayDialog()
        {
            if (viewGameplay != null)
                viewGameplay.Close();
        }
        public void ShowStatsDialog()
        {
            viewAdvancedStats = new ViewAdvancedStats();
            viewAdvancedStats.ShowDialog();
        }

        public void CloseStatsDialog()
        {
            if (viewAdvancedStats != null)
                viewAdvancedStats.Close();
        }
    }
}
