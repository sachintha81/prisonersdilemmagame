﻿using PrisonersDilemma.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrisonersDilemma
{
    public class PDViewModelLocator
    {
        private static ViewModelPDMain viewModelPDMain = new ViewModelPDMain();

        private static ViewModelStrategyOptions viewModelStrategyOptions = new ViewModelStrategyOptions();

        private static ViewModelGlobalSettings viewModelGlobalSettings = new ViewModelGlobalSettings();

        private static ViewModelGameplay viewModelGameplay = new ViewModelGameplay();

        private static ViewModelAdvancedStats viewModelAdvancedStats = new ViewModelAdvancedStats();

        public static ViewModelPDMain ViewModelPDMain
        {
            get
            {
                return viewModelPDMain;
            }
        }

        public static ViewModelStrategyOptions ViewModelStrategyOptions
        {
            get
            {
                return viewModelStrategyOptions;
            }
        }

        public static ViewModelGlobalSettings ViewModelGlobalSettings
        {
            get
            {
                return viewModelGlobalSettings;
            }
        }

        public static ViewModelGameplay ViewModelGameplay
        {
            get
            {
                return viewModelGameplay;
            }
        }

        public static ViewModelAdvancedStats ViewModelAdvancedStats
        {
            get
            {
                return viewModelAdvancedStats;
            }
        }
    }
}
