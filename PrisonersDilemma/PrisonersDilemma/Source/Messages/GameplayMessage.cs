﻿using PrisonersDilemma.Model;
using PrisonersDilemma.StrategyCommon;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace PrisonersDilemma.Messages
{
    /// <summary>
    /// Message type for communication between the Main and GlobalSettings views.
    /// </summary>
    public class GameplayMessage
    {
        public GameplayMessage()
        {
            pointScheme = new PointScheme();
            strategies = new ObservableCollection<Strategy>();
        }

        private PointScheme pointScheme;

        private ObservableCollection<Strategy> strategies;

        public PointScheme PointScheme
        {
            get
            {
                return pointScheme;
            }

            set
            {
                pointScheme = value;
            }
        }

        public ObservableCollection<Strategy> Strategies
        {
            get
            {
                return strategies;
            }

            set
            {
                strategies = value;
            }
        }

        public void AddStrategies(Strategy strategy)
        {
            strategies.Add(strategy);
        }
    }
}
