﻿using PrisonersDilemma.Model;
using PrisonersDilemma.StrategyCommon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrisonersDilemma.Messages
{
    /// <summary>
    /// Message type for communication between the Main and GlobalSettings views.
    /// </summary>
    public class SettingsMessage
    {
        public SettingsMessage()
        {
            pointScheme = new PointScheme();
            optionStrategies = new List<Strategy>();
        }

        private PointScheme pointScheme;

        private List<Strategy> optionStrategies;

        public PointScheme PointScheme
        {
            get
            {
                return pointScheme;
            }

            set
            {
                pointScheme = value;
            }
        }

        public List<Strategy> OptionStrategies
        {
            get
            {
                return optionStrategies;
            }

            set
            {
                optionStrategies = value;
            }
        }

        public void AddStrategies(Strategy strategy)
        {
            optionStrategies.Add(strategy);
        }
    }
}
