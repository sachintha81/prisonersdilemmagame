﻿using PrisonersDilemma.Model;
using PrisonersDilemma.StrategyCommon;
using System.Collections.Generic;

namespace PrisonersDilemma.Messages
{
    /// <summary>
    /// Message type for communication between the Gameplay and AdvancedStats views.
    /// </summary>
    public class StatsMessage
    {
        public StatsMessage()
        {
            iterations = 0;
            pointScheme = new PointScheme();
            result = new GameResult();
        }

        private int iterations;

        private PointScheme pointScheme;

        private GameResult result;

        public int Iterations
        {
            get
            {
                return iterations;
            }

            set
            {
                iterations = value;
            }
        }

        public PointScheme PointScheme
        {
            get
            {
                return pointScheme;
            }

            set
            {
                pointScheme = value;
            }
        }

        public GameResult Result
        {
            get
            {
                return result;
            }

            set
            {
                result = value;
            }
        }
    }
}
