﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace PrisonersDilemma.Utility
{
    /// <summary>
    /// Does not convert values.
    /// Used for debugging purposes.
    /// </summary>
    public class DoNothingConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value;
        }
    }
}
