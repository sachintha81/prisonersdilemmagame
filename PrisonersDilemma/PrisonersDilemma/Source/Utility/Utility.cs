﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Media;
using System.Runtime.Serialization.Formatters.Binary;
using PrisonersDilemma.Common;
using PrisonersDilemma.Model;
using PrisonersDilemma.StrategyCommon;
using static PrisonersDilemma.Common.Common;
using System.Windows.Forms;

namespace PrisonersDilemma.Utility
{
    /// <summary>
    /// Utility class that provide common utiliti methods for others.
    /// </summary>
    public static class Utility
    {
        private static string INFO_FILE_SAVE_SUCCESS = "File was saved successfully!";
        private static string ERROR_FILE_SAVE_FAILED = "Failed to save the file!";

        public static bool LoadPointScheme(out PointScheme pointScheme, out string msg)
        {
            bool ret = true;
            msg = string.Empty;
            pointScheme = new PointScheme();

            try
            {
                pointScheme.Reward = Properties.Settings.Default.Reward;
                pointScheme.Punishment = Properties.Settings.Default.Punishment;
                pointScheme.Temptation = Properties.Settings.Default.Temptation;
                pointScheme.Payoff = Properties.Settings.Default.Payoff;
            }
            catch (Exception ex)
            {
                msg = ex.Message;
                ret = false;
            }

            return ret;
        }

        public static bool SavePointScheme(PointScheme pointScheme, out string msg)
        {
            bool ret = true;
            msg = string.Empty;

            try
            {
                Properties.Settings.Default.Reward = pointScheme.Reward;
                Properties.Settings.Default.Punishment = pointScheme.Punishment;
                Properties.Settings.Default.Temptation = pointScheme.Temptation;
                Properties.Settings.Default.Payoff = pointScheme.Payoff;

                Properties.Settings.Default.Save();
            }
            catch (Exception ex)
            {
                msg = ex.Message;
                ret = false;
            }
            return ret;
        }

        public static bool LoadDllClassesToList(string directory, PointScheme pointScheme, out ObservableCollection<Strategy> dllList, out string msg)
        {
            bool ret = true;
            msg = string.Empty;
            dllList = null;

            try
            {
                dllList = new ObservableCollection<Strategy>();
                string[] files = Directory.GetFiles(directory, "*.dll", SearchOption.TopDirectoryOnly);

                foreach (string file in files)
                {
                    string strDllPath = Path.GetFullPath(file);
                    if (File.Exists(strDllPath))
                    {
                        // Execute the method from the requested .dll using reflection (System.Reflection).
                        Assembly DLL = Assembly.LoadFrom(strDllPath);

                        foreach (Type type in DLL.GetTypes())
                        {
                            if (!type.IsClass || type.IsInterface || type.IsNotPublic || (!type.Name.StartsWith("PD")))
                                continue;

                            object classInst = Activator.CreateInstance(type);

                            MethodInfo getStratInfoMethod = type.GetMethod(Method_GetStrategyInfo);
                            StrategyInfo result = (StrategyInfo)getStratInfoMethod.Invoke(classInst, null);

                            MethodInfo setPointSchemeMethod = type.GetMethod(Method_SetPoints);
                            object[] points = new object[1] { (object)pointScheme };
                            setPointSchemeMethod.Invoke(classInst, points);

                            Strategy st = new Strategy(new StrategyInfo(
                                                                            result.StrategyName,
                                                                            result.StrategyType,
                                                                            result.Description,
                                                                            result.HasOptions), classInst, type);
                            dllList.Add(st);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                msg = ex.Message;
                ret = false;
            }

            return ret;
        }

        public static bool SaveGameResult(GameResult result, GameStats s1, GameStats s2, out string msg)
        {
            bool ret = true;
            msg = string.Empty;

            try
            {
                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.DefaultExt = ".txt";
                saveFileDialog.OverwritePrompt = true;
                saveFileDialog.AddExtension = true;

                if (saveFileDialog.ShowDialog() == DialogResult.OK)
                {
                    string content = Gameplay.PrintGame(result, s1, s2);
                    FileStream fs = new FileStream(saveFileDialog.FileName, FileMode.Create, FileAccess.Write);
                    StreamWriter sw = new StreamWriter(fs);
                    sw.Write(content);
                    sw.Close();
                    fs.Close();
                    msg = INFO_FILE_SAVE_SUCCESS;
                    ret = true;
                }
                else
                {
                    msg = ERROR_FILE_SAVE_FAILED;
                    ret = false;
                }
            }
            catch (Exception ex)
            {
                msg = ex.Message;
                ret = false;
            }

            return ret;
        }

        public static bool ComputeStats(GameResult result, out GameStats s1Stats, out GameStats s2Stats, out string msg)
        {
            bool ret = true;
            msg = string.Empty;
            s1Stats = new GameStats();
            s2Stats = new GameStats();

            int cCount1 = 0;
            int cPoints1 = 0;
            double cAvg1 = 0;
            int dCount1 = 0;
            int dPoints1 = 0;
            double dAvg1 = 0;

            int cCount2 = 0;
            int cPoints2 = 0;
            double cAvg2 = 0;
            int dCount2 = 0;
            int dPoints2 = 0;
            double dAvg2 = 0;

            try
            {
                pointsForMoveType(result.History, out cCount1, out cPoints1, out cAvg1, out dCount1, out dPoints1, out dAvg1,
                                                  out cCount2, out cPoints2, out cAvg2, out dCount2, out dPoints2, out dAvg2);
                s1Stats.NumC = cCount1;
                s1Stats.TotC = cPoints1;
                s1Stats.AvgC = cAvg1;

                s1Stats.NumD = dCount1;
                s1Stats.TotD = dPoints1;
                s1Stats.AvgD = dAvg1;

                s2Stats.NumC = cCount2;
                s2Stats.TotC = cPoints2;
                s2Stats.AvgC = cAvg2;

                s2Stats.NumD = dCount2;
                s2Stats.TotD = dPoints2;
                s2Stats.AvgD = dAvg2;

                s1Stats.Total = result.Player1Points;
                s1Stats.Average = (double)result.Player1Points / result.Iterations;
                s1Stats.Average = Math.Round(s1Stats.Average, 2);
                s2Stats.Total = result.Player2Points;
                s2Stats.Average = (double)result.Player2Points / result.Iterations;
                s2Stats.Average = Math.Round(s2Stats.Average, 2);
            }
            catch (Exception ex)
            {
                msg = ex.Message;
                ret = false;
            }

            return ret;
        }

        private static void pointsForMoveType(List<HistoryItem> history, out int cCount1, out int cPoints1, out double cAvg1, out int dCount1, out int dPoints1, out double dAvg1,
                                                                  out int cCount2, out int cPoints2, out double cAvg2, out int dCount2, out int dPoints2, out double dAvg2)
        {
            cCount1 = 0;
            cPoints1 = 0;
            cAvg1 = 0;

            dCount1 = 0;
            dPoints1 = 0;
            dAvg1 = 0;

            cCount2 = 0;
            cPoints2 = 0;
            cAvg2 = 0;

            dCount2 = 0;
            dPoints2 = 0;
            dAvg2 = 0;

            foreach (HistoryItem item in history)
            {
                switch (item.Card1)
                {
                    case CommonTypes.Card.Cooperate:
                        cPoints1 += item.Point1;
                        cCount1++;
                        break;
                    case CommonTypes.Card.Defect:
                        dPoints1 += item.Point1;
                        dCount1++;
                        break;
                }

                switch (item.Card2)
                {
                    case CommonTypes.Card.Cooperate:
                        cPoints2 += item.Point2;
                        cCount2++;
                        break;
                    case CommonTypes.Card.Defect:
                        dPoints2 += item.Point2;
                        dCount2++;
                        break;
                }
            }

            if (cCount1 != 0)
                cAvg1 = (double)cPoints1 / cCount1;

            if (dCount1 != 0)
                dAvg1 = (double)dPoints1 / dCount1;

            if (cCount2 != 0)
                cAvg2 = (double)cPoints2 / cCount2;

            if (dCount2 != 0)
                dAvg2 = (double)dPoints2 / dCount2;

            cAvg1 = Math.Round(cAvg1, 2);
            dAvg1 = Math.Round(dAvg1, 2);
            cAvg2 = Math.Round(cAvg2, 2);
            dAvg2 = Math.Round(dAvg2, 2);
        }

        public static IEnumerable<T> FindVisualChildren<T>(DependencyObject depObj) where T : DependencyObject
        {
            if (depObj != null)
            {
                for (int i = 0; i < VisualTreeHelper.GetChildrenCount(depObj); i++)
                {
                    DependencyObject child = VisualTreeHelper.GetChild(depObj, i);
                    if (child != null && child is T)
                    {
                        yield return (T)child;
                    }

                    foreach (T childOfChild in FindVisualChildren<T>(child))
                    {
                        yield return childOfChild;
                    }
                }
            }
        }

        /// <summary>
        /// Makes a new copy of an object without keeping a reference.
        /// </summary>
        /// <typeparam name="T">Type of object.</typeparam>
        /// <param name="a">Object to copy</param>
        /// <returns>A new copy of an object of type T</returns>
        /// <remarks>The class of the object must be marked as [Serializable] to be able to DeepClone.</remarks>
        public static T DeepClone<T>(this T a)
        {
            using (MemoryStream stream = new MemoryStream())
            {
                BinaryFormatter formatter = new BinaryFormatter();
                formatter.Serialize(stream, a);
                stream.Position = 0;
                return (T)formatter.Deserialize(stream);
            }
        }
    }
}
