﻿using PrisonersDilemma.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PrisonersDilemma.View
{
    /// <summary>
    /// Interaction logic for ViewGameplay.xaml
    /// </summary>
    public partial class ViewGameplay : Window
    {
        public ViewGameplay()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Initialize();
        }

        private void Initialize()
        {
            if (cmbStrat1 != null && cmbStrat1.Items.Count != 0)
                cmbStrat1.SelectedIndex = 0;
            if (cmbStrat2 != null && cmbStrat2.Items.Count != 0)
                cmbStrat2.SelectedIndex = 0;

            WpfScreen currentScreen = WpfScreen.GetScreenFrom(this);
            Rect screenRect = currentScreen.DeviceBounds;
            this.MaxHeight = screenRect.Height - 100;
        }
    }
}
