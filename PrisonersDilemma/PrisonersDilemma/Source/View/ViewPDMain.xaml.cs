﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PrisonersDilemma.View
{
    /// <summary>
    /// Interaction logic for ViewPDMain.xaml
    /// </summary>
    public partial class ViewPDMain : Window
    {
        public ViewPDMain()
        {
            InitializeComponent();
        }

        private void btnExit_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void btnHelp_Click(object sender, RoutedEventArgs e)
        {
        }
    }
}
