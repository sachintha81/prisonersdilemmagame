﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PrisonersDilemma.View
{
    /// <summary>
    /// Interaction logic for ViewStrategyOptions.xaml
    /// </summary>
    public partial class ViewStrategyOptions : Window
    {
        public ViewStrategyOptions()
        {
            InitializeComponent();
        }

        private void FixWindowSizeToContent()
        {
            this.MinHeight = this.ActualHeight;
            this.MinWidth = this.ActualWidth;
            this.MaxHeight = this.ActualHeight;
            this.MaxWidth = this.ActualWidth;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            FixWindowSizeToContent();
        }
    }
}
