﻿using PrisonersDilemma.Common;
using PrisonersDilemma.Messages;
using PrisonersDilemma.Model;
using PrisonersDilemma.StrategyCommon;
using PrisonersDilemma.Utility;
using PrisonersDilemma.View;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;

namespace PrisonersDilemma.ViewModel
{
    public class ViewModelAdvancedStats : INotifyPropertyChanged
    {
        #region PropertyChanged handled
        public event PropertyChangedEventHandler PropertyChanged;
        private void RaisePropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion

        #region Const Strings
        private const string ERROR_NO_RESULTS = "No results found.";

        #endregion

        /// <summary>
        /// Initializes local results, history, Strat1 and Strate2 stats instances.
        /// </summary>
        public ViewModelAdvancedStats()
        {
            Result = new GameResult();
            History = new List<HistoryItem>();
            Strat1Stats = new GameStats();
            Strat2Stats = new GameStats();

            string msg = string.Empty;
            if (!LoadCommands(out msg))
            {
                MessageBox.Show(msg, Error.ERROR, MessageBoxButton.OK, MessageBoxImage.Error);
            }

            StatsMessenger.Default.Register<StatsMessage>(this, OnResultsReceived);
        }

        /// <summary>
        /// When results are received, compute stats.
        /// </summary>
        /// <param name="obj"></param>
        private void OnResultsReceived(StatsMessage obj)
        {
            string msg = string.Empty;

            try
            {
                result = obj.Result;
                history = obj.Result.History;
                pointScheme = obj.PointScheme;
                iterations = obj.Iterations;
                // Display the used point scheme.
                pointsString = string.Format("{0}, {1}, {2}, {3}", pointScheme.Reward,
                                                                    pointScheme.Punishment,
                                                                    pointScheme.Temptation,
                                                                    pointScheme.Payoff);
                if (!Utility.Utility.ComputeStats(result, out strat1Stats, out strat2Stats, out msg))
                {
                    MessageBox.Show(msg, Error.ERROR, MessageBoxButton.OK, MessageBoxImage.Error);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, Error.ERROR, MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        #region Private Variables
        private GameResult result = null;
        private List<HistoryItem> history = null;
        PointScheme pointScheme = null;
        private string pointsString = string.Empty;
        private int iterations = 0;
        private GameStats strat1Stats;
        private GameStats strat2Stats;
        #endregion

        #region Properties

        public GameResult Result
        {
            get
            {
                return result;
            }

            set
            {
                result = value;
                RaisePropertyChanged("Result");
            }
        }

        public List<HistoryItem> History
        {
            get
            {
                return history;
            }

            set
            {
                history = value;
                RaisePropertyChanged("History");
            }
        }

        public string PointsString
        {
            get
            {
                return pointsString;
            }

            set
            {
                pointsString = value;
                RaisePropertyChanged("PointsString");
            }
        }

        public int Iterations
        {
            get
            {
                return iterations;
            }

            set
            {
                iterations = value;
                RaisePropertyChanged("Iterations");
            }
        }

        public GameStats Strat1Stats
        {
            get
            {
                return strat1Stats;
            }

            set
            {
                strat1Stats = value;
                RaisePropertyChanged("Strat1Stats");
            }
        }

        public GameStats Strat2Stats
        {
            get
            {
                return strat2Stats;
            }

            set
            {
                strat2Stats = value;
                RaisePropertyChanged("Strat2Stats");
            }
        }

        #endregion

        #region Data Binding

        public ICommand SaveButtonClickedCommand { get; set; }
        public ICommand ExitButtonClickedCommand { get; set; }

        /// <summary>
        /// Load custom commands.
        /// </summary>
        /// <param name="msg"></param>
        /// <returns></returns>
        private bool LoadCommands(out string msg)
        {
            bool ret = true;
            msg = string.Empty;

            try
            {
                SaveButtonClickedCommand = new CustomCommand(SaveResults, CanSaveResults);
                ExitButtonClickedCommand = new CustomCommand(ExitWindow, CanExitWindow);
            }
            catch (Exception ex)
            {
                msg = ex.Message;
                ret = false;
            }

            return ret;
        }

        /// <summary>
        /// Save results to a file.
        /// </summary>
        /// <param name="obj"></param>
        private void SaveResults(object obj)
        {
            string msg = string.Empty;
            try
            {
                bool success = Utility.Utility.SaveGameResult(result, strat1Stats, strat2Stats, out msg);
                if (success)
                    MessageBox.Show(msg, Error.SUCCESS, MessageBoxButton.OK, MessageBoxImage.Information);
                else
                    MessageBox.Show(msg, Error.ERROR, MessageBoxButton.OK, MessageBoxImage.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, Error.ERROR, MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private bool CanSaveResults(object obj)
        {
            // To save results, there must be results and computed statistics for the results.
            if (result != null && history != null && history.Count != 0 && strat1Stats != null && strat2Stats != null)
            {
                return true;
            }
            else
            {
                MessageBox.Show(ERROR_NO_RESULTS, Error.ERROR, MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }
        }

        /// <summary>
        /// Exit the window.
        /// </summary>
        /// <param name="obj"></param>
        private void ExitWindow(object obj)
        {
            Window thisWindow = null;
            foreach (Window window in Application.Current.Windows.OfType<ViewAdvancedStats>())
            {
                thisWindow = (ViewAdvancedStats)window;
                break;
            }
            if (thisWindow != null)
            {
                thisWindow.Close();
            }
        }

        private bool CanExitWindow(object obj)
        {
            return true;
        }
        #endregion

        #region Private Methods
        #endregion
    }
}
