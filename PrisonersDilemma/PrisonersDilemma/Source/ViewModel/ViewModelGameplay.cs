﻿using PrisonersDilemma.Common;
using PrisonersDilemma.LocalServices;
using PrisonersDilemma.Messages;
using PrisonersDilemma.Model;
using PrisonersDilemma.StrategyCommon;
using PrisonersDilemma.Utility;
using PrisonersDilemma.View;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;

namespace PrisonersDilemma.ViewModel
{
    public class ViewModelGameplay : INotifyPropertyChanged
    {
        #region PropertyChanged handled
        public event PropertyChangedEventHandler PropertyChanged;
        private void RaisePropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion

        #region Const Strings
        private const string ERROR_STRAT_NOT_RECEIVED = "No strategies were received.";
        private const string ERROR_STRAT_NOT_SELECTED = "Please select two Strategies to being.";
        private const string ERROR_NO_STATS = "Please run the game to display statistics.";
        private const string ERROR_NO_RESULTS = "No results found.";
        #endregion

        /// <summary>
        /// Initializations and register to receive GameplayMessage.
        /// </summary>
        public ViewModelGameplay()
        {
            Initialize();

            GameplayMessenger.Default.Register<GameplayMessage>(this, OnStrategiesReceived);

            dialogService = new DialogService();
        }

        /// <summary>
        /// When a GameplayMessage received, check for validity and if OK, save to local variables.
        /// </summary>
        /// <param name="obj"></param>
        private void OnStrategiesReceived(GameplayMessage obj)
        {
            try
            {
                if (obj == null || obj.Strategies == null || obj.Strategies.Count == 0 || obj.PointScheme == null)
                {
                    MessageBox.Show(ERROR_STRAT_NOT_RECEIVED, Error.ERROR, MessageBoxButton.OK, MessageBoxImage.Error);
                }
                else
                {
                    dllList = obj.Strategies;
                    pointScheme.Reward = obj.PointScheme.Reward;
                    pointScheme.Punishment = obj.PointScheme.Punishment;
                    pointScheme.Temptation = obj.PointScheme.Temptation;
                    pointScheme.Payoff = obj.PointScheme.Payoff;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, Error.ERROR, MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        #region Private Variables

        private ObservableCollection<Strategy> dllList = null;

        private PointScheme pointScheme = null;

        private Strategy selectedStrategy1 = null;

        private Strategy selectedStrategy2 = null;

        private int iterations = 0;

        private GameResult result = null;

        private List<HistoryItem> history = null;

        private int strat1Points = 0;

        private int strat2Points = 0;

        private GameStats strat1Stats = null;

        private GameStats strat2Stats = null;

        DialogService dialogService = null;
        #endregion

        #region Properties

        public ObservableCollection<Strategy> DllList
        {
            get
            {
                return dllList;
            }

            set
            {
                dllList = value;
                RaisePropertyChanged("DllList");
            }
        }

        public List<HistoryItem> History
        {
            get
            {
                return history;
            }

            set
            {
                history = value;
                RaisePropertyChanged("History");
            }
        }

        public Strategy SelectedStrategy1
        {
            get
            {
                return selectedStrategy1;
            }

            set
            {
                selectedStrategy1 = value;
                RaisePropertyChanged("SelectedStrategy1");
            }
        }

        public Strategy SelectedStrategy2
        {
            get
            {
                return selectedStrategy2;
            }

            set
            {
                selectedStrategy2 = value;
                RaisePropertyChanged("SelectedStrategy2");
            }
        }

        public int Iterations
        {
            get
            {
                return iterations;
            }

            set
            {
                iterations = value;
                RaisePropertyChanged("Iterations");
            }
        }

        public GameResult Results
        {
            get
            {
                return result;
            }

            set
            {
                result = value;
                RaisePropertyChanged("Results");
            }
        }

        public int Strat1Points
        {
            get
            {
                return strat1Points;
            }

            set
            {
                strat1Points = value;
                RaisePropertyChanged("Strat1Points");
            }
        }

        public int Strat2Points
        {
            get
            {
                return strat2Points;
            }

            set
            {
                strat2Points = value;
                RaisePropertyChanged("Strat2Points");
            }
        }

        #endregion

        #region Data Binding

        public ICommand SelectionChangedCommand1 { get; set; }
        public ICommand SelectionChangedCommand2 { get; set; }
        public ICommand IterationSelectedCommand { get; set; }
        public ICommand PlayButtonClickedCommand { get; set; }
        public ICommand StatButtonClickedCommand { get; set; }
        public ICommand SettingsButtonClickedCommand { get; set; }
        public ICommand SaveButtonClickedCommand { get; set; }
        public ICommand ExitButtonClickedCommand { get; set; }

        /// <summary>
        /// Load custom commands.
        /// </summary>
        /// <param name="msg">Error, if any.</param>
        /// <returns>Success/Failure : True/False</returns>
        private bool LoadCommands(out string msg)
        {
            bool ret = true;
            msg = string.Empty;

            try
            {
                SelectionChangedCommand1 = new CustomCommand(ChangeSelection1, CanChangeSelection1);
                SelectionChangedCommand2 = new CustomCommand(ChangeSelection2, CanChangeSelection2);
                IterationSelectedCommand = new CustomCommand(ChangeIteration, CanChangeIteration);
                PlayButtonClickedCommand = new CustomCommand(PlayGame, CanCanPlayGame);
                StatButtonClickedCommand = new CustomCommand(OpenStats, CanOpenStats);
                SettingsButtonClickedCommand = new CustomCommand(OpenSettings, CanOpenSettings);
                SaveButtonClickedCommand = new CustomCommand(SaveResults, CanSaveResults);
                ExitButtonClickedCommand = new CustomCommand(ExitGame, CanExitGame);
            }
            catch (Exception ex)
            {
                msg = ex.Message;
                ret = false;
            }

            return ret;
        }

        /// <summary>
        /// Save results to a file.
        /// </summary>
        /// <param name="obj"></param>
        private void SaveResults(object obj)
        {
            string msg = string.Empty;
            try
            {
                // Compute stats before saving.
                if (Utility.Utility.ComputeStats(result, out strat1Stats, out strat2Stats, out msg))
                {
                    Utility.Utility.SaveGameResult(result, strat1Stats, strat2Stats, out msg);
                }
                else
                {
                    MessageBox.Show(msg, Error.ERROR, MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, Error.ERROR, MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private bool CanSaveResults(object obj)
        {
            // To save, there must be results.
            if (result != null && history != null && history.Count != 0)
            {
                return true;
            }
            else
            {
                MessageBox.Show(ERROR_NO_RESULTS, Error.ERROR, MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }
        }

        /// <summary>
        /// Plays Prisoner's Dilemma, one strategy against another one.
        /// </summary>
        /// <param name="obj"></param>
        private void PlayGame(object obj)
        {
            try
            {
                if (SelectedStrategy1 == null || SelectedStrategy2 == null)
                    MessageBox.Show(ERROR_STRAT_NOT_SELECTED, Error.ERROR, MessageBoxButton.OK, MessageBoxImage.Error);
                else
                {
                    Exception ex = null;
                    Gameplay.PlayOneVsOne(pointScheme, SelectedStrategy1, SelectedStrategy2, Iterations, out result, out ex);
                    Strat1Points = result.Player1Points;
                    Strat2Points = result.Player2Points;
                    History = result.History;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, Error.ERROR, MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private bool CanCanPlayGame(object obj)
        {
            // To play, two strategies must be selected.
            if (SelectedStrategy1 != null && SelectedStrategy2 != null)
                return true;
            else
                return false;
        }

        private void ChangeSelection1(object obj)
        {
            Strat1Points = 0;
            Strat2Points = 0;
        }

        private bool CanChangeSelection1(object obj)
        {
            return true;
        }

        private void ChangeSelection2(object obj)
        {
            Strat1Points = 0;
            Strat2Points = 0;
        }

        private bool CanChangeSelection2(object obj)
        {
            return true;
        }

        private void ChangeIteration(object obj)
        {
        }

        private bool CanChangeIteration(object obj)
        {
            return true;
        }

        /// <summary>
        /// Opends the advanced statistics window.
        /// </summary>
        /// <param name="obj"></param>
        private void OpenStats(object obj)
        {
            StatsMessage statsMsg = new StatsMessage();
            statsMsg.Iterations = result.Iterations;
            statsMsg.PointScheme = pointScheme;
            statsMsg.Result = result;

            StatsMessenger.Default.Send(statsMsg);
            dialogService.ShowStatsDialog();
        }

        private bool CanOpenStats(object obj)
        {
            // To open stats, there must be results.
            if (result != null && result.History != null && result.History.Count != 0)
                return true;
            else
                MessageBox.Show(ERROR_NO_STATS, Error.INFO, MessageBoxButton.OK, MessageBoxImage.Information);
            return false;
        }

        /// <summary>
        /// Opens Global Settings window.
        /// </summary>
        /// <param name="obj"></param>
        private void OpenSettings(object obj)
        {
            try
            {
                SettingsMessage settings = new SettingsMessage();
                settings.PointScheme = pointScheme;
                foreach (Strategy strategy in dllList)
                {
                    if (strategy.StratInfo.HasOptions)
                    {
                        settings.AddStrategies(strategy);
                    }
                }
                SettingsMessenger.Default.Send(settings);
                dialogService.ShowSettingsDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, Error.ERROR, MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private bool CanOpenSettings(object obj)
        {
            return true;
        }

        private void ExitGame(object obj)
        {
            Window thisWindow = null;
            foreach (Window window in Application.Current.Windows.OfType<ViewGameplay>())
            {
                thisWindow = (ViewGameplay)window;
                break;
            }
            if (thisWindow != null)
            {
                Reset();
                thisWindow.Close();
            }
        }

        private bool CanExitGame(object obj)
        {
            return true;
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Initializations: initiate Dll list, point scheme and history (results) list.
        /// Default iterations = 10
        /// </summary>
        private void Initialize()
        {
            try
            {
                string msg = string.Empty;
                dllList = new ObservableCollection<Strategy>();
                pointScheme = new PointScheme();
                history = new List<HistoryItem>();
                iterations = 10;

                if (!LoadCommands(out msg))
                {
                    MessageBox.Show(msg, Error.ERROR, MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, Error.ERROR, MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// Clear everything when exiting.
        /// </summary>
        private void Reset()
        {
            dllList = null;
            selectedStrategy1 = null;
            selectedStrategy2 = null;
            iterations = 10;
            result = null;
            history.Clear();
            history = null;
            strat1Points = 0;
            strat2Points = 0;
        }

        #endregion
    }
}
