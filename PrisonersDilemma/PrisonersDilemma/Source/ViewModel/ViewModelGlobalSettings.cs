﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;
using System.Linq;
using System.Windows.Controls;
using System.Reflection;
// PrisonersDilemma
using PrisonersDilemma.Utility;
using PrisonersDilemma.View;
using PrisonersDilemma.LocalServices;
using PrisonersDilemma.Messages;
using PrisonersDilemma.Model;
using PrisonersDilemma.StrategyCommon;
using PrisonersDilemma.Common;

namespace PrisonersDilemma.ViewModel
{
    public class ViewModelGlobalSettings : INotifyPropertyChanged
    {
        #region PropertyChanged handled
        public event PropertyChangedEventHandler PropertyChanged;
        private void RaisePropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion

        #region Const Strings

        private string INFO_POINT_RELATIONSHIP = "The four categories must adhere to the following scheme:"
                                                + Environment.NewLine
                                                + "Temptation > Reward > Punishment > Payoff";
        private string INFO_POINT_SAVE_SUCCESS = "New Point Scheme was saved successfully";

        private string CAPTION_POINT_SCHEME = "Point Scheme";

        private string ERROR_INVALID_POINT_SCHEME = "The Point Scheme must follow the "
                                                + Environment.NewLine + "Temptation > Reward > Punishment > Payoff relationship."
                                                + Environment.NewLine + "Are you sure you want to continue?";

        private const string ERROR_STRAT_NOT_SELECTED = "Please select a Strategy";
        private const string ERROR_SAVE_FAILED = "Failed to save to the file.";

        #endregion

        /// <summary>
        /// Instantiate the StackPanel to which controls are added dynamically.
        /// Loads custom commands.
        /// Register for incoming SettingsMessage type.
        /// </summary>
        public ViewModelGlobalSettings()
        {
            try
            {
                string msg = string.Empty;
                SpChildItems = new List<StackPanel>();
                if (!LoadCommands(out msg))
                {
                    MessageBox.Show(msg, Error.ERROR, MessageBoxButton.OK, MessageBoxImage.Error);
                }
                SettingsMessenger.Default.Register<SettingsMessage>(this, OnSettingsReceived);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, Error.ERROR, MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// Set local PointScheme and OptionStrategies.
        /// </summary>
        /// <param name="obj"></param>
        private void OnSettingsReceived(SettingsMessage obj)
        {
            try
            {
                PointScheme = obj.PointScheme;
                OptionStrategies = obj.OptionStrategies;

                reward = obj.PointScheme.Reward;
                punishment = obj.PointScheme.Punishment;
                temptation = obj.PointScheme.Temptation;
                payoff = obj.PointScheme.Payoff;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, Error.ERROR, MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        #region Private Variables

        int reward;
        int punishment;
        int temptation;
        int payoff;

        DialogService dialogService = null;

        private PointScheme pointScheme;

        private List<Strategy> optionStrategies;

        private Strategy selectedStrategy;

        private List<StackPanel> spChildItems = null;

        private int gridRowCount = 0;

        #endregion

        #region Properties

        public PointScheme PointScheme
        {
            get
            {
                return pointScheme;
            }

            set
            {
                pointScheme = value;
                RaisePropertyChanged("PointScheme");
            }
        }

        public List<Strategy> OptionStrategies
        {
            get
            {
                return optionStrategies;
            }

            set
            {
                optionStrategies = value;
                RaisePropertyChanged("OptionStrategies");
            }
        }

        public Strategy SelectedStrategy
        {
            get
            {
                return selectedStrategy;
            }

            set
            {
                selectedStrategy = value;
                RaisePropertyChanged("SelectedStrategy");
            }
        }

        public List<StackPanel> SpChildItems
        {
            get
            {
                return spChildItems;
            }

            set
            {
                spChildItems = value;
                RaisePropertyChanged("SpChildItems");
            }
        }

        public int GridRowCount
        {
            get
            {
                return gridRowCount;
            }

            set
            {
                gridRowCount = value;
                RaisePropertyChanged("GridRowCount");
            }
        }

        #endregion

        #region Data Binding

        public ICommand InfoCommand { get; set; }
        public ICommand SavePointsCommand { get; set; }
        public ICommand EditOptionCommand { get; set; }
        public ICommand SaveAllCommand { get; set; }
        public ICommand CancelCommand { get; set; }
        public ICommand SelectionChangedCommand { get; set; }

        /// <summary>
        /// Loads custom commands.
        /// </summary>
        /// <param name="msg">Error, if any.</param>
        /// <returns>Success/Failure : True/False</returns>
        private bool LoadCommands(out string msg)
        {
            bool ret = true;
            msg = string.Empty;

            try
            {
                InfoCommand = new CustomCommand(DisplayInfo, CanDisplayInfo);
                SavePointsCommand = new CustomCommand(SavePoints, CanSavePoints);
                EditOptionCommand = new CustomCommand(EditOption, CanEditOption);
                SaveAllCommand = new CustomCommand(SaveAll, CanSaveAll);
                CancelCommand = new CustomCommand(Cancel, CanCancel);
                SelectionChangedCommand = new CustomCommand(SelectionChanged, CanChangeSelection);
            }
            catch (Exception ex)
            {
                msg = ex.Message;
                ret = false;
            }

            return ret;
        }

        /// <summary>
        /// ListBox selection changed event.
        /// </summary>
        /// <param name="obj"></param>
        private void SelectionChanged(object obj)
        {
        }

        private bool CanChangeSelection(object obj)
        {
            return true;
        }

        /// <summary>
        /// Exit without saving if Cancel is pressed.
        /// </summary>
        /// <param name="obj"></param>
        private void Cancel(object obj)
        {
            try
            {
                Window thisWindow = null;
                foreach (Window window in Application.Current.Windows.OfType<ViewGlobalSettings>())
                {
                    thisWindow = (ViewGlobalSettings)window;
                    break;
                }
                SettingsMessage settings = new SettingsMessage();
                PointScheme originalPointScheme = new PointScheme(reward, punishment, temptation, payoff);
                settings.PointScheme = originalPointScheme;
                foreach (Strategy strategy in optionStrategies)
                {
                    settings.AddStrategies(strategy);
                }
                SettingsMessenger.Default.Send(settings);

                thisWindow.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, Error.ERROR, MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private bool CanCancel(object obj)
        {
            return true;
        }

        /// <summary>
        /// Save All button click.
        /// Save both PointScheme and OptionStrategies.
        /// *** Haven't implemented the OptionStrategies save yet. ***
        /// </summary>
        /// <param name="obj"></param>
        private void SaveAll(object obj)
        {
            try
            {
                if (SavePointScheme())
                {
                    Window thisWindow = null;
                    foreach (Window window in Application.Current.Windows.OfType<ViewGlobalSettings>())
                    {
                        thisWindow = (ViewGlobalSettings)window;
                        break;
                    }
                    thisWindow.Close();
                }
                else
                {
                    MessageBox.Show(ERROR_SAVE_FAILED, Error.ERROR, MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, Error.ERROR, MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private bool CanSaveAll(object obj)
        {
            return true;
        }

        /// <summary>
        /// Edit button click.
        /// *** Hasn't implemented properly yet. Button is disabled so this won't get executed yet. ***
        /// </summary>
        /// <param name="obj"></param>
        private void EditOption(object obj)
        {
            try
            {
                if (SelectedStrategy != null)
                {
                    object instance = SelectedStrategy.InstanceObj;
                    Type type = SelectedStrategy.TypeObj;

                    MethodInfo getInfo = type.GetMethod(Common.Common.Method_GetStrategyInfo);
                    object info = getInfo.Invoke(instance, null);
                    MethodInfo getOpt = type.GetMethod(Common.Common.Method_GetOptions);
                    object options = getOpt.Invoke(instance, null);

                    StrategyOptions strategyOptions = new StrategyOptions();
                    strategyOptions.SetStrategyOptions(info, options);
                    OptionsMessenger.Default.Send(strategyOptions);
                    dialogService.ShowOptionsDialog();
                }
                else
                {
                    MessageBox.Show(ERROR_STRAT_NOT_SELECTED, Error.ERROR, MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, Error.ERROR, MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private bool CanEditOption(object obj)
        {
            return true;
        }

        /// <summary>
        /// Save the edited point scheme.
        /// </summary>
        /// <param name="obj"></param>
        private void SavePoints(object obj)
        {
            try
            {
                SavePointScheme();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, Error.ERROR, MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private bool CanSavePoints(object obj)
        {
            return true;
        }

        /// <summary>
        /// Display information about the strategy.
        /// </summary>
        /// <param name="obj"></param>
        private void DisplayInfo(object obj)
        {
            MessageBox.Show(INFO_POINT_RELATIONSHIP, CAPTION_POINT_SCHEME, MessageBoxButton.OK, MessageBoxImage.Information);
        }

        private bool CanDisplayInfo(object obj)
        {
            return true;
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Saves the point scheme to App.config
        /// </summary>
        /// <returns></returns>
        private bool SavePointScheme()
        {
            bool ret = false;
            if (!CheckPointSchemeValidity())
            {
                if (MessageBoxResult.Yes == MessageBox.Show(ERROR_INVALID_POINT_SCHEME, Error.WARNING, MessageBoxButton.YesNo, MessageBoxImage.Exclamation))
                {
                    ret = Save();
                }
            }
            else
            {
                ret = Save();
            }
            return ret;
        }

        private bool Save()
        {
            bool ret = false;
            string msg = string.Empty;

            try
            {
                if (Utility.Utility.SavePointScheme(pointScheme, out msg))
                {
                    reward = pointScheme.Reward;
                    punishment = pointScheme.Punishment;
                    temptation = pointScheme.Temptation;
                    payoff = pointScheme.Payoff;
                    MessageBox.Show(INFO_POINT_SAVE_SUCCESS, Error.SUCCESS, MessageBoxButton.OK, MessageBoxImage.Information);
                    ret = true;
                }
                else
                {
                    MessageBox.Show(msg, Error.ERROR, MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, Error.ERROR, MessageBoxButton.OK, MessageBoxImage.Error);
            }

            return ret;
        }

        /// <summary>
        /// Check if the point scheme follows the Temptation > Reward > Punishment > Payoff rule.
        /// </summary>
        /// <returns></returns>
        private bool CheckPointSchemeValidity()
        {
            bool isValid = false;
            if (pointScheme.Temptation > pointScheme.Reward
                    && pointScheme.Reward > pointScheme.Punishment
                    && pointScheme.Punishment > pointScheme.Payoff)
            {
                isValid = true;
            }
            return isValid;
        }

        #endregion
    }
}
