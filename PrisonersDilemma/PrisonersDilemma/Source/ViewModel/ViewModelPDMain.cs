﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Input;
using System.Reflection;
// PrisonersDilemma
using PrisonersDilemma.Utility;
using PrisonersDilemma.Model;
using PrisonersDilemma.StrategyCommon;
using PrisonersDilemma.LocalServices;
using PrisonersDilemma.Common;
using PrisonersDilemma.Messages;
using static PrisonersDilemma.Utility.Utility;
using static PrisonersDilemma.StrategyCommon.CommonTypes;
using static PrisonersDilemma.Common.Common;

namespace PrisonersDilemma.ViewModel
{
    public class ViewModelPDMain : INotifyPropertyChanged
    {
        #region NotifyProperty handled
        /// <summary>
        /// Implementation of INotifyPropertyChanged
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        private void RaisePropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion

        #region Const Strings

        private const string ERROR_STRAT_NOT_SELECTED = "Please select a Strategy";
        private const string ERROR_NO_STRATS = "No Prisoner's Dilemma Strategies were found.";
        private const string INFO_DLL_LOCATION = "Strategy libraries (DLLs) must be placed in the same directory where the PrisonersDilemma.exe executable is located.";

        #endregion

        public ViewModelPDMain()
        {
            bool ret = false;
            string msg = string.Empty;

            ret = LoadCommands(out msg);
            if (ret)
            {
                ret = Initialize(out msg);
                if (ret)
                {
                    // Display, on screen, the no. of strategies of each type; Nice, Mean, and Other.
                    ret = CountStrategyTypes(out msg);
                    if (ret)
                    {
                        // Register for types of messages to recieve from child windows.
                        OptionsMessenger.Default.Register<StrategyOptions>(this, OnUpdatedStrategyOptionsReceived);
                        SettingsMessenger.Default.Register<SettingsMessage>(this, OnUpdatedSettingsReceived);
                    }
                }
            }

            if (!ret)
                MessageBox.Show(msg, Error.ERROR, MessageBoxButton.OK, MessageBoxImage.Error);
        }

        /// <summary>
        /// Receive changed Strategy options from StrategyOptions windows.
        /// Save the received values to the DLL list in memory.
        /// </summary>
        /// <param name="obj">Changed StrategyOptions</param>
        private void OnUpdatedStrategyOptionsReceived(StrategyOptions obj)
        {
            try
            {
                // Set the changed option values to the correct DLL (in memory).
                object instance = SelectedStrategy.InstanceObj;
                Type type = SelectedStrategy.TypeObj;
                MethodInfo setOptions = type.GetMethod(Method_SetOptions);
                List<OptionInfo> optList = obj.GetStrategyOptions();
                object[] parameters = new object[] { optList };
                object info = setOptions.Invoke(instance, parameters);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, Error.ERROR, MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// Save updated Global Settings to the local copy in memory.
        /// Changes to the settings files are done in the ViewGlobalSettings window.
        /// </summary>
        /// <param name="obj">Changed settings</param>
        private void OnUpdatedSettingsReceived(SettingsMessage obj)
        {
            try
            {
                pointScheme = obj.PointScheme;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, Error.ERROR, MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        #region Private Variables

        private int totalStrats = 0;
        private int niceStrats = 0;
        private int meanStrats = 0;
        private int otherStrats = 0;
        private Visibility optBtnVisibility = Visibility.Collapsed;
        private Visibility warningIconVisibility = Visibility.Collapsed;
        private DialogService dialogService;
        private ObservableCollection<Strategy> dllList = null;
        private PointScheme pointScheme = null;
        private Strategy selectedStrategy = null;
        private StrategyOptions strategyOptions = null;
        #endregion

        #region Properties

        /// <summary>
        /// Copies of DLLs read from the HD.
        /// Type is ObservableCollection because it can be kept updates continuously.
        /// </summary>
        public ObservableCollection<Strategy> DllList
        {
            get
            {
                return dllList;
            }

            set
            {
                dllList = value;
                RaisePropertyChanged("DllList");
            }
        }

        /// <summary>
        /// Currently selected strategy.
        /// </summary>
        public Strategy SelectedStrategy
        {
            get
            {
                return selectedStrategy;
            }

            set
            {
                selectedStrategy = value;
                RaisePropertyChanged("SelectedStrategy");
            }
        }

        public StrategyOptions StrategyOptions
        {
            get
            {
                return strategyOptions;
            }

            set
            {
                strategyOptions = value;
                RaisePropertyChanged("StrategyOptions");
            }
        }

        public int TotalStrats
        {
            get
            {
                return totalStrats;
            }

            set
            {
                totalStrats = value;
                RaisePropertyChanged("TotalStrats");
            }
        }

        public int NiceStrats
        {
            get
            {
                return niceStrats;
            }

            set
            {
                niceStrats = value;
                RaisePropertyChanged("NiceStrats");
            }
        }

        public int MeanStrats
        {
            get
            {
                return meanStrats;
            }

            set
            {
                meanStrats = value;
                RaisePropertyChanged("MeanStrats");
            }
        }

        public int OtherStrats
        {
            get
            {
                return otherStrats;
            }

            set
            {
                otherStrats = value;
                RaisePropertyChanged("OtherStrats");
            }
        }

        public Visibility OptBtnVisibility
        {
            get
            {
                return optBtnVisibility;
            }

            set
            {
                optBtnVisibility = value;
                RaisePropertyChanged("OptBtnVisibility");
            }
        }

        public Visibility WarningIconVisibility
        {
            get
            {
                return warningIconVisibility;
            }

            set
            {
                warningIconVisibility = value;
                RaisePropertyChanged("WarningIconVisibility");
            }
        }

        #endregion

        #region Data Binding

        public ICommand SelectionChangedCommand { get; set; }
        public ICommand OpenStratOptionsCommand { get; set; }
        public ICommand OpenSettingsCommand { get; set; }
        public ICommand OpenGameplayCommand { get; set; }
        public ICommand InfoCommand { get; set; }
        public ICommand WindowLoadedCommand { get; set; }

        /// <summary>
        /// Loads the custom commands before the window loads.
        /// </summary>
        /// <param name="msg">Error message in case of an error.</param>
        /// <returns></returns>
        private bool LoadCommands(out string msg)
        {
            bool ret = true;
            msg = string.Empty;

            try
            {
                SelectionChangedCommand = new CustomCommand(ChangeSelection, CanChangeSelection);
                OpenStratOptionsCommand = new CustomCommand(OpenStratOptions, CanOpenStratOptions);
                OpenSettingsCommand = new CustomCommand(OpenSettings, CanOpenSettings);
                OpenGameplayCommand = new CustomCommand(OpenGameplay, CanOpenGameplay);
                InfoCommand = new CustomCommand(OpenInfo, CanOpenInfo);
                WindowLoadedCommand = new CustomCommand(WindowLoaded, CanLoadWindow);
            }
            catch (Exception ex)
            {
                msg = ex.Message;
                ret = false;
            }

            return ret;
        }

        /// <summary>
        /// If no strategy DLLs were found at the loading time, display a warning button.
        /// Message display is handled in 'OpenInfo' event.
        /// </summary>
        /// <param name="obj"></param>
        private void WindowLoaded(object obj)
        {
            if(DllList==null || DllList.Count == 0)
            {
                WarningIconVisibility = Visibility.Visible;
            }
            else
            {
                WarningIconVisibility = Visibility.Collapsed;
            }
        }

        private bool CanLoadWindow(object obj)
        {
            return true;
        }

        /// <summary>
        /// Display a message in case no strategies were loaded.
        /// </summary>
        /// <param name="obj"></param>
        private void OpenInfo(object obj)
        {
            MessageBox.Show(INFO_DLL_LOCATION, Error.INFO, MessageBoxButton.OK, MessageBoxImage.Information);
        }

        private bool CanOpenInfo(object obj)
        {
            return true;
        }

        /// <summary>
        /// Opens Settings window when [Settings] button is clicked.
        /// </summary>
        /// <param name="obj"></param>
        private void OpenSettings(object obj)
        {
            try
            {
                SettingsMessage settings = new SettingsMessage();
                settings.PointScheme = pointScheme;
                foreach (Strategy strategy in dllList)
                {
                    if (strategy.StratInfo.HasOptions)
                    {
                        settings.AddStrategies(strategy);
                    }
                }
                SettingsMessenger.Default.Send(settings);
                dialogService.ShowSettingsDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, Error.ERROR, MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private bool CanOpenSettings(object obj)
        {
            return true;
        }

        /// <summary>
        /// Opens the Strategy Options window.
        /// Only for strategies which have changeable options.
        /// </summary>
        /// <param name="obj"></param>
        private void OpenStratOptions(object obj)
        {
            try
            {
                if (SelectedStrategy != null)
                {
                    object instance = SelectedStrategy.InstanceObj;
                    Type type = SelectedStrategy.TypeObj;

                    // Retrieve information about strategy: Strategy name, type, description etc.
                    MethodInfo getInfo = type.GetMethod(Method_GetStrategyInfo);
                    object info = getInfo.Invoke(instance, null);
                    // If it has changeable options, retrieve information about those options.
                    MethodInfo getOpt = type.GetMethod(Method_GetOptions);
                    object options = getOpt.Invoke(instance, null);

                    // Open a new window where user can change the options.
                    strategyOptions = new StrategyOptions();
                    strategyOptions.SetStrategyOptions(info, options);
                    OptionsMessenger.Default.Send(strategyOptions);
                    dialogService.ShowOptionsDialog();
                }
                else
                {
                    MessageBox.Show(ERROR_STRAT_NOT_SELECTED, Error.ERROR, MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, Error.ERROR, MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private bool CanOpenStratOptions(object obj)
        {
            if (OptBtnVisibility.Equals(Visibility.Visible))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// SelectionChanged event of the ListBox.
        /// </summary>
        /// <param name="obj"></param>
        private void ChangeSelection(object obj)
        {
            try
            {
                if (SelectedStrategy != null)
                {
                    Strategy selectedStrat = SelectedStrategy;

                    if (selectedStrat.StratInfo.HasOptions)
                    {
                        OptBtnVisibility = Visibility.Visible;
                    }
                    else
                    {
                        OptBtnVisibility = Visibility.Collapsed;
                    }
                }
                else
                {
                    MessageBox.Show(ERROR_STRAT_NOT_SELECTED, Error.ERROR, MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, Error.ERROR, MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private bool CanChangeSelection(object obj)
        {
            return true;
        }

        /// <summary>
        /// Opens Gameplay window.
        /// </summary>
        /// <param name="obj"></param>
        private void OpenGameplay(object obj)
        {
            if (DllList != null && DllList.Count != 0)
            {
                // Send the current list of strategies and point scheme
                GameplayMessage gameplay = new GameplayMessage();
                gameplay.PointScheme = pointScheme;
                foreach (Strategy strat in DllList)
                {
                    gameplay.AddStrategies(strat);
                }

                GameplayMessenger.Default.Send(gameplay);
                dialogService.ShowGameplayyDialog();
            }
            else
            {
                MessageBox.Show(ERROR_NO_STRATS, Error.ERROR, MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private bool CanOpenGameplay(object obj)
        {
            return true;
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Initializations.
        /// Instantiate dialog services, dll list, point scheme etc.
        /// Currently, DLLs are read from the current directory (of the executable.)
        /// Will changet later so that user can select the dir.
        /// </summary>
        private bool Initialize(out string msg)
        {
            bool ret = true;
            msg = string.Empty;

            try
            {
                dialogService = new DialogService();
                dllList = new ObservableCollection<Strategy>();
                pointScheme = new PointScheme();
                selectedStrategy = new Strategy(new StrategyInfo(string.Empty, StrategyTypes.None, "Please select a strategy to see the description.", false), null, null);
                if (!LoadPointScheme(out pointScheme, out msg))
                {
                    ret = false;
                }
                else
                {
                    string exeDir = Directory.GetCurrentDirectory();
                    string dllDir = exeDir;
                    //string dllDir = Path.Combine(exeDir, "..\\..\\..\\..\\PrisonersDilemmaStrategies\\Libraries");
                    if (!LoadDllClassesToList(dllDir, pointScheme, out dllList, out msg))
                    {
                        WarningIconVisibility = Visibility.Visible;
                        // Set to true temporarily because otherwise this generates a MsgBox at compile time.
                        ret = true;
                        //ret = false;
                    }
                    else
                    {
                        WarningIconVisibility = Visibility.Collapsed;
                    }
                }
            }
            catch (Exception ex)
            {
                msg = ex.Message;
                ret = false;
            }

            return ret;
        }

        /// <summary>
        /// Count different types of strategies.
        /// </summary>
        private bool CountStrategyTypes(out string msg)
        {
            bool ret = true;
            msg = string.Empty;

            try
            {
                TotalStrats = 0;
                NiceStrats = 0;
                MeanStrats = 0;
                OtherStrats = 0;

                for (int i = 0; i < DllList.Count; i++)
                {
                    switch (DllList[i].StratInfo.StrategyType)
                    {
                        case StrategyTypes.Nice:
                            NiceStrats++;
                            break;
                        case StrategyTypes.Mean:
                            MeanStrats++;
                            break;
                        case StrategyTypes.None:
                            OtherStrats++;
                            break;
                    }
                }

                TotalStrats = DllList.Count;
            }
            catch (Exception e)
            {
                msg = e.Message;
                ret = false;
            }

            return ret;
        }

        #endregion
    }
}