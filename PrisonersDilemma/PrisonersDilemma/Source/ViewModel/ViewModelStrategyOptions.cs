﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
// PrisonersDilemma
using PrisonersDilemma.View;
using PrisonersDilemma.StrategyCommon;
using PrisonersDilemma.Utility;
using PrisonersDilemma.Common;
using static PrisonersDilemma.Utility.Utility;

namespace PrisonersDilemma.ViewModel
{
    public class ViewModelStrategyOptions : INotifyPropertyChanged
    {
        #region PropertyChanged handled
        public event PropertyChangedEventHandler PropertyChanged;
        private void RaisePropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion

        #region Const Strings
        private const string TXTBOX_PREFIX = "txtOpt_";

        private const string CAPTION_INVALID_VALUE = "Invalid Value";
        private const string ERROR_INVALID_VAL = "Invalid value. Please enter a correct value.";

        private string ERROR_VALUE_TOO_SMALL = string.Format("Entered value is smaller than the allowed minimum.{0}Please enter a valid value.", Environment.NewLine);
        private string ERROR_VALUE_TOO_LARGE = string.Format("Entered value is larger than the allowed maximum.{0}Please enter a valid value.", Environment.NewLine);
        #endregion

        /// <summary>
        /// Instantiate the StackPanel to which controls are added dynamically.
        /// Register to receive a message of tye StrategyOptions
        /// </summary>
        public ViewModelStrategyOptions()
        {
            SpChildItems = new List<StackPanel>();

            string msg = string.Empty;
            if (!LoadCommands(out msg))
            {
                MessageBox.Show(msg, Error.ERROR, MessageBoxButton.OK, MessageBoxImage.Error);
            }

            OptionsMessenger.Default.Register<StrategyOptions>(this, OnStrategyReceived);
        }

        /// <summary>
        /// When a message of type StrategyOptionns received, add controls dynamically.
        /// </summary>
        /// <param name="options"></param>
        private void OnStrategyReceived(StrategyOptions options)
        {
            bool ret = false;
            string msg = string.Empty;

            if (options != null)
            {
                SelectedOptions = options;
                ret = AddOptionControls(out msg);
            }

            if (!ret)
                MessageBox.Show(msg, Common.Error.ERROR, MessageBoxButton.OK, MessageBoxImage.Error);
        }

        #region Private Variables

        private StrategyOptions selectedOptions = null;

        private string strategyName = string.Empty;

        private int gridRowCount = 0;

        private List<StackPanel> spChildItems = null;

        #endregion

        #region Properties

        public StrategyOptions SelectedOptions
        {
            get
            {
                return selectedOptions;
            }

            set
            {
                selectedOptions = value;
                RaisePropertyChanged("SelectedOptions");
            }
        }

        public string StrategyName
        {
            get
            {
                return strategyName;
            }

            set
            {
                strategyName = value;
                RaisePropertyChanged("StrategyName");
            }
        }

        public int GridRowCount
        {
            get
            {
                return gridRowCount;
            }

            set
            {
                gridRowCount = value;
                RaisePropertyChanged("GridRowCount");
            }
        }

        public List<StackPanel> SpChildItems
        {
            get
            {
                return spChildItems;
            }

            set
            {
                spChildItems = value;
                RaisePropertyChanged("SpChildItems");
            }
        }

        #endregion

        #region Data Binding

        public ICommand SaveCommand { get; set; }
        public ICommand CancelCommand { get; set; }
        public ICommand InfoCommand { get; set; }

        /// <summary>
        /// Instantiate custom commands.
        /// </summary>
        /// <param name="msg"></param>
        /// <returns></returns>
        private bool LoadCommands(out string msg)
        {
            bool ret = true;
            msg = string.Empty;
            try
            {
                SaveCommand = new CustomCommand(SaveOptions, CanSaveOptions);
                CancelCommand = new CustomCommand(CancelOptions, CanCancelOptions);
                InfoCommand = new CustomCommand(DisplayInfo, CanDisplayInfo);
            }
            catch (Exception ex)
            {
                msg = ex.Message;
                ret = false;
            }
            return ret;
        }

        /// <summary>
        /// Save options when user change options and hit the Save button.
        /// </summary>
        /// <param name="obj"></param>
        private void SaveOptions(object obj)
        {
            bool ret = false;

            // Find the current window.
            Window thisWindow = null;
            foreach (Window window in Application.Current.Windows.OfType<ViewStrategyOptions>())
            {
                thisWindow = (ViewStrategyOptions)window;
                break;
            }

            // Find textboxes in the current window.
            foreach (TextBox tb in FindVisualChildren<TextBox>(thisWindow))
            {
                if (tb.Name.StartsWith(TXTBOX_PREFIX))
                {
                    string msg = string.Empty;
                    int id = Convert.ToInt32(tb.Tag);
                    double value;
                    if (!ValidateInput(tb.Text, id, out value, out msg))
                    {
                        MessageBox.Show(msg, CAPTION_INVALID_VALUE, MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                    else
                    {
                        // If textboxes have valid values, save them.
                        ret = SelectedOptions.SetIndividualItem(id, value, ref msg);
                        if (!ret)
                        {
                            MessageBox.Show(msg, CAPTION_INVALID_VALUE, MessageBoxButton.OK, MessageBoxImage.Error);
                        }
                    }
                }
            }

            if (ret)
            {
                // Send the return message type and close the window.
                OptionsMessenger.Default.Send(selectedOptions);
                thisWindow.Close();
            }
        }

        private bool CanSaveOptions(object obj)
        {
            return true;
        }

        /// <summary>
        /// If user press Cancel button, close the window without changing values.
        /// </summary>
        /// <param name="obj"></param>
        private void CancelOptions(object obj)
        {
            try
            {
                Window thisWindow = null;
                foreach (Window window in Application.Current.Windows.OfType<ViewStrategyOptions>())
                {
                    thisWindow = (ViewStrategyOptions)window;
                    break;
                }
                OptionsMessenger.Default.Send(selectedOptions);
                thisWindow.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, Error.ERROR, MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private bool CanCancelOptions(object obj)
        {
            return true;
        }

        /// <summary>
        /// Display information about the strategy.
        /// </summary>
        /// <param name="obj"></param>
        private void DisplayInfo(object obj)
        {
            MessageBox.Show(selectedOptions.Info.Description, string.Format("{0} Description", selectedOptions.Info.StrategyName), MessageBoxButton.OK, MessageBoxImage.Information);
        }

        private bool CanDisplayInfo(object obj)
        {
            return true;
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Validate input values of the textboxes.
        /// </summary>
        /// <param name="textVal">Text box value</param>
        /// <param name="id">Option ID.</param>
        /// <param name="value">Convert value from strinng to double.</param>
        /// <param name="msg">Error, if any.</param>
        /// <returns>Success/Failure : True / False</returns>
        private bool ValidateInput(string textVal, int id, out double value, out string msg)
        {
            bool ret = true;
            msg = string.Empty;
            value = 0;

            try
            {
                if (!double.TryParse(textVal, out value))
                {
                    msg = ERROR_INVALID_VAL;
                    ret = false;
                }
                else
                {
                    OptionInfo option = selectedOptions.GetStrategyOptions().Find(x => x.OptId == id);
                    double min = (double)option.MinValue;
                    double max = (double)option.MaxValue;
                    if (min >= 0 && value < min)
                    {
                        msg = ERROR_VALUE_TOO_SMALL;
                        ret = false;
                    }
                    else if (max >= 0 && value > max)
                    {
                        msg = ERROR_VALUE_TOO_LARGE;
                        ret = false;
                    }
                }
            }
            catch (Exception ex)
            {
                msg = ex.Message;
                ret = false;
            }

            return ret;
        }

        /// <summary>
        /// Add controls (label and textbox) dynamically.
        /// </summary>
        /// <param name="msg">Error, if any.</param>
        /// <returns>Success/Failure : True/False</returns>
        public bool AddOptionControls(out string msg)
        {
            bool ret = false;
            msg = string.Empty;

            if (SelectedOptions != null)
            {
                try
                {
                    StrategyName = SelectedOptions.Info.StrategyName;

                    // Clearing items from a possible previous call.
                    SpChildItems = new List<StackPanel>();

                    for (int i = 0; i < SelectedOptions.GetStrategyOptions().Count; i++)
                    {
                        GridRowCount += 1;

                        int optId = SelectedOptions.GetStrategyOptions()[i].OptId;
                        string optName = SelectedOptions.GetStrategyOptions()[i].OptName;
                        string optType = SelectedOptions.GetStrategyOptions()[i].OptType;
                        object optValue = SelectedOptions.GetStrategyOptions()[i].OptValue;

                        StackPanel spOneRow = new StackPanel();
                        spOneRow.Orientation = Orientation.Horizontal;
                        spOneRow.HorizontalAlignment = HorizontalAlignment.Center;
                        spOneRow.VerticalAlignment = VerticalAlignment.Center;

                        // Adding option name labels
                        Grid gridLeft = new Grid();
                        gridLeft.Name = string.Format("gridOptLabel_{0}", optName.Replace(" ", string.Empty));
                        Grid.SetIsSharedSizeScope(gridLeft, true);
                        ColumnDefinition cdLeft = new ColumnDefinition();
                        cdLeft.Width = GridLength.Auto;
                        cdLeft.SharedSizeGroup = "SharedLeft";
                        gridLeft.ColumnDefinitions.Add(cdLeft);

                        Label lbl = new Label();
                        lbl.Name = string.Format("lblOpt_{0}", optName.Replace(" ", string.Empty));
                        lbl.Content = string.Format("{0} ({1}) : ", optName, optType);
                        lbl.Margin = new Thickness(2);
                        lbl.MaxHeight = 25.96;
                        lbl.MinWidth = 100;
                        gridLeft.Children.Add(lbl);
                        Grid.SetColumn(gridLeft, 0);
                        Grid.SetRow(gridLeft, i);
                        spOneRow.Children.Add(gridLeft);

                        // Adding textboxes for option values
                        Grid gridRight = new Grid();
                        gridRight.Name = string.Format("gridOptValue_{0}", optName.Replace(" ", string.Empty));
                        Grid.SetIsSharedSizeScope(gridRight, true);
                        ColumnDefinition cdRight = new ColumnDefinition();
                        cdRight.Width = GridLength.Auto;
                        cdRight.SharedSizeGroup = "SharedRight";
                        gridRight.ColumnDefinitions.Add(cdRight);

                        TextBox txt = new TextBox();
                        txt.Name = string.Format("{0}{1}", TXTBOX_PREFIX, optName.Replace(" ", string.Empty));
                        txt.Tag = optId;
                        txt.Text = SelectedOptions.GetStrategyOptions()[i].OptValue.ToString();
                        txt.Margin = new Thickness(2);
                        txt.MinWidth = 100;
                        txt.MaxHeight = 25.96;
                        txt.VerticalContentAlignment = VerticalAlignment.Center;
                        gridRight.Children.Add(txt);
                        Grid.SetColumn(gridRight, 1);
                        Grid.SetRow(gridRight, i);
                        spOneRow.Children.Add(gridRight);
                        SpChildItems.Add(spOneRow);
                    }
                    ret = true;
                }
                catch (Exception e)
                {
                    msg = e.Message;
                    ret = false;
                }
            }
            return ret;
        }

        #endregion
    }
}