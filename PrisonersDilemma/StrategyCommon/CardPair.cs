﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static PrisonersDilemma.StrategyCommon.CommonTypes;

namespace PrisonersDilemma.StrategyCommon
{
    public class CardPair
    {
        public CardPair(Card p1, Card p2)
        {
            player1 = p1;
            player2 = p2;
        }

        private Card player1;

        public Card Player1
        {
            get
            {
                return player1;
            }
            set
            {
                player1 = value;
            }
        }

        private Card player2;

        public Card Player2
        {
            get
            {
                return player2;
            }
            set
            {
                player2 = value;
            }
        }
    }
}
