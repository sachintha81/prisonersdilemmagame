﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrisonersDilemma.StrategyCommon
{
    public class CommonTypes
    {
        public enum MinMax
        {
            NO_MIN = -1,
            NO_MAX = -2
        }

        public enum Card
        {
            None = 0,
            Cooperate = 1,
            Defect = 2
        }

        public enum PointCategories
        {
            Reward,
            Punishment,
            Temptation,
            Payoff
        }

        public enum StrategyTypes
        {
            None = 0,
            Nice = 1,
            Mean = 2,
        }

        public enum TypesOfOpponents
        {
            Cooperative, AllD, STFT, Random
        }
    }
}
