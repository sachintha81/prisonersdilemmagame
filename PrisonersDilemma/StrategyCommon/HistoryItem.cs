﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static PrisonersDilemma.StrategyCommon.CommonTypes;

namespace PrisonersDilemma.StrategyCommon
{
    public class HistoryItem
    {
        private int turn;
        private Card card1;
        private Card card2;
        private int point1;
        private int point2;

        public int Turn
        {
            get
            {
                return turn;
            }

            set
            {
                turn = value;
            }
        }

        public Card Card1
        {
            get
            {
                return card1;
            }

            set
            {
                card1 = value;
            }
        }

        public Card Card2
        {
            get
            {
                return card2;
            }

            set
            {
                card2 = value;
            }
        }

        public int Point1
        {
            get
            {
                return point1;
            }

            set
            {
                point1 = value;
            }
        }

        public int Point2
        {
            get
            {
                return point2;
            }

            set
            {
                point2 = value;
            }
        }

        public HistoryItem()
        {
            turn = -1;
            card1 = Card.None;
            card2 = Card.None;
            point1 = -1;
            point2 = -1;
        }

        public HistoryItem(int pTurn, Card p1Card, Card p2Card, int p1Point, int p2Point)
        {
            turn = pTurn;
            card1 = p1Card;
            card2 = p2Card;
            point1 = p1Point;
            point2 = p2Point;
        }
    }
}
