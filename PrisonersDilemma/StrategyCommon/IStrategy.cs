﻿using PrisonersDilemma.StrategyCommon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static PrisonersDilemma.StrategyCommon.CommonTypes;

namespace StrategyCommon
{
    public interface IStrategy
    {
        StrategyInfo GetStrategyInfo();

        List<OptionInfo> GetOptions();

        void SetOptions(List<OptionInfo> options);

        void SetPointScheme(PointScheme pointScheme);

        bool PlayCard(
                        int turn,
                        List<Card> movesHistorySelf,
                        List<Card> movesHistoryOpponent,
                        List<int> pointHistorySelf,
                        List<int> pointHistoryOpponent, out Card card);

        bool Flush();
    }
}
