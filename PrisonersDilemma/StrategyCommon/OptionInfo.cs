﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrisonersDilemma.StrategyCommon
{
    public class OptionInfo
    {
        private int optId;
        private string optName;
        private string optType;
        private object optValue;
        private object minValue;
        private object maxValue;

        #region Properties
        public int OptId
        {
            get
            {
                return optId;
            }

            set
            {
                optId = value;
            }
        }

        public string OptName
        {
            get
            {
                return optName;
            }

            set
            {
                optName = value;
            }
        }

        public string OptType
        {
            get
            {
                return optType;
            }

            set
            {
                optType = value;
            }
        }

        public object OptValue
        {
            get
            {
                return optValue;
            }

            set
            {
                optValue = value;
            }
        }

        public object MinValue
        {
            get
            {
                return minValue;
            }

            set
            {
                minValue = value;
            }
        }

        public object MaxValue
        {
            get
            {
                return maxValue;
            }

            set
            {
                maxValue = value;
            }
        }
        #endregion

        public OptionInfo()
        {

        }

        public OptionInfo(int id, string name, string type, object value, object minVal, object maxVal)
        {
            optId = id;
            optName = name;
            optType = type;
            optValue = value;
            minValue = minVal;
            maxValue = maxVal;
        }
    }
}
