﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrisonersDilemma.StrategyCommon
{
    public class PointScheme
    {
        private int reward;
        private int punishment;
        private int temptation;
        private int payoff;

        #region Properties
        public int Reward
        {
            get
            {
                return reward;
            }

            set
            {
                reward = value;
            }
        }

        public int Punishment
        {
            get
            {
                return punishment;
            }

            set
            {
                punishment = value;
            }
        }

        public int Temptation
        {
            get
            {
                return temptation;
            }

            set
            {
                temptation = value;
            }
        }

        public int Payoff
        {
            get
            {
                return payoff;
            }

            set
            {
                payoff = value;
            }
        }
        #endregion

        public PointScheme()
        {

        }

        public PointScheme(int rewardVal, int punishmentVal, int temptationVal, int payoffVal)
        {
            reward = rewardVal;
            punishment = punishmentVal;
            temptation = temptationVal;
            payoff = payoffVal;
        }
    }
}
