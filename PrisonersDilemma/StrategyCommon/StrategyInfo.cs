﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static PrisonersDilemma.StrategyCommon.CommonTypes;

namespace PrisonersDilemma.StrategyCommon
{
    public class StrategyInfo
    {
        private string strategyName;
        private StrategyTypes strategyType;
        private string description;
        private bool hasOptions;

        #region Properties
        public string StrategyName
        {
            get
            {
                return strategyName;
            }

            set
            {
                strategyName = value;
            }
        }

        public StrategyTypes StrategyType
        {
            get
            {
                return strategyType;
            }

            set
            {
                strategyType = value;
            }
        }

        public string Description
        {
            get
            {
                return description;
            }

            set
            {
                description = value;
            }
        }

        public bool HasOptions
        {
            get
            {
                return hasOptions;
            }

            set
            {
                hasOptions = value;
            }
        }
        #endregion

        public StrategyInfo()
        {

        }

        public StrategyInfo(string sName, StrategyTypes sType, string sDescription, bool sHasOptions)
        {
            strategyName = sName;
            strategyType = sType;
            description = sDescription;
            hasOptions = sHasOptions;
        }
    }
}
