﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static PrisonersDilemma.StrategyCommon.CommonTypes;

namespace PrisonersDilemma.StrategyCommon
{
    public class StrategyOptions
    {
        private string MSG_VALUE_OUT_OF_RANGE = "Input value is not within allowed range."
                                                + Environment.NewLine
                                                + "Please enter a valid value."
                                                + Environment.NewLine
                                                + "Click information button for more information";

        private StrategyInfo info;

        private List<OptionInfo> options = null;

        public StrategyInfo Info
        {
            get { return info; }
            set { info = value; }
        }

        public StrategyOptions()
        {
            options = new List<OptionInfo>();
        }

        public void SetStrategyOptions(object stratInfo, object optionsList)
        {
            StrategyInfo sInfo = (StrategyInfo)stratInfo;
            info = new StrategyInfo();
            info.StrategyName = sInfo.StrategyName;
            info.StrategyType = sInfo.StrategyType;
            info.Description = sInfo.Description;
            info.HasOptions = sInfo.HasOptions;

            List<OptionInfo> list = (List<OptionInfo>)optionsList;
            foreach (OptionInfo opt in list)
            {
                options.Add(opt);
            }
        }

        public List<OptionInfo> GetStrategyOptions()
        {
            return options;
        }

        public bool SetIndividualItem(int id, object value, ref string message)
        {
            bool ret = true;

            int index = options.FindIndex(x => x.OptId.Equals(id));
            OptionInfo opt = options[index];
            double min = Convert.ToDouble(opt.MinValue);
            double max = Convert.ToDouble(opt.MaxValue);
            if ((min != (Convert.ToDouble(MinMax.NO_MIN))) && (Convert.ToDouble(value) < min))
            {
                message = MSG_VALUE_OUT_OF_RANGE;
                ret = false;
            }
            else if ((max != (Convert.ToDouble(MinMax.NO_MAX))) && (Convert.ToDouble(value) > max))
            {
                message = MSG_VALUE_OUT_OF_RANGE;
                ret = false;
            }
            else
            {
                opt.OptValue = value;
                options[index] = opt;
            }

            return ret;
        }

        public bool ClearOptions()
        {
            bool ret = true;
            try
            {
                options = null;
            }
            catch
            {
                ret = false;
            }
            return ret;
        }
    }
}