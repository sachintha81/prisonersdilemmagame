﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StrategyCommon;
using PrisonersDilemma.StrategyCommon;
using static PrisonersDilemma.StrategyCommon.CommonTypes;

namespace PrisonersDilemmaStrategies
{
    /// <summary>
    /// Starts with D,D,D,C.
    /// If the opponent plays the same sequence of D,D,D,C, it cooperates until the opponent defects. 
    /// Otherwise, it defects until the opponent defects on continuous three moves, and then it cooperates on the following move.
    /// </summary>
    public class PDMeanFortress4 : IStrategy
    {
        private StrategyInfo info;

        public StrategyInfo GetStrategyInfo()
        {
            return info;
        }

        public List<OptionInfo> GetOptions()
        {
            return null;
        }

        public void SetOptions(List<OptionInfo> options)
        {
            ;
        }

        public void SetPointScheme(PointScheme pointScheme)
        {
            Reward = pointScheme.Reward;
            Punishment = pointScheme.Punishment;
            Temptation = pointScheme.Temptation;
            Payoff = pointScheme.Payoff;
        }

        private bool isKin = false;

        private int Reward = 0;
        private int Punishment = 0;
        private int Temptation = 0;
        private int Payoff = 0;

        public PDMeanFortress4()
        {
            info = new StrategyInfo();
            info.StrategyName = "Fortress4";
            info.StrategyType = StrategyTypes.Mean;
            info.Description = string.Empty;
            info.Description += "Starts with D,D,D,C." + Environment.NewLine;
            info.Description += "If the opponent plays the same sequence of D,D,D,C, it cooperates until the opponent defects." + Environment.NewLine;
            info.Description += "Otherwise, it defects until the opponent defects on continuous three moves, and then it cooperates on the following move.";
            info.HasOptions = false;
        }

        public bool PlayCard(
                                int turn,
                                List<Card> movesHistorySelf,
                                List<Card> movesHistoryOpponent,
                                List<int> pointHistorySelf,
                                List<int> pointHistoryOpponent, out Card card)
        {
            bool ret = true;
            card = Card.Defect;
            int lastIndex = 0;

            try
            {
                if (turn == 1 || turn == 2 || turn == 3)
                {
                    card = Card.Defect;
                }
                else if (turn == 4)
                {
                    card = Card.Cooperate;
                }
                else if (turn == 5)
                {
                    if (movesHistoryOpponent[0] == movesHistorySelf[0] &&
                        movesHistoryOpponent[1] == movesHistorySelf[1] &&
                        movesHistoryOpponent[2] == movesHistorySelf[2] &&
                        movesHistoryOpponent[3] == movesHistorySelf[3])
                    {
                        isKin = true;
                    }
                    else
                    {
                        isKin = false;
                    }
                }

                if (turn >= 5)
                {
                    lastIndex = movesHistoryOpponent.Count - 1;

                    if (isKin)
                    {
                        // Cooperates until the opponent defects.
                        if (movesHistoryOpponent[lastIndex] == Card.Cooperate)
                        {
                            card = Card.Cooperate;
                        }
                        else
                        {
                            card = Card.Defect;
                        }
                    }
                    else
                    {
                        // Defects until the opponent defects on continuous two moves, 
                        // and then it cooperates on the following move.
                        if (movesHistoryOpponent[lastIndex] == Card.Defect && movesHistoryOpponent[lastIndex - 1] == Card.Defect && movesHistoryOpponent[lastIndex - 2] == Card.Defect)
                        {
                            card = Card.Cooperate;
                        }
                        else
                        {
                            card = Card.Defect;
                        }
                    }
                }
            }
            catch
            {
                ret = false;
            }
            return ret;
        }

        public bool Flush()
        {
            isKin = false;
            return true;
        }
    }
}
