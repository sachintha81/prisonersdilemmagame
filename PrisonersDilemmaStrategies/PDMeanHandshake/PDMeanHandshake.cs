﻿using PrisonersDilemma.StrategyCommon;
using StrategyCommon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StrategyCommon;
using PrisonersDilemma.StrategyCommon;
using static PrisonersDilemma.StrategyCommon.CommonTypes;

namespace PrisonersDilemmaStrategies
{
    /// <summary>
    /// Defects on the first move.
    /// Cooperates on the second move.
    /// If the opponent behaves the same way --> Cooperate thereafter.
    /// Otherwise --> Defect thereater.
    /// </summary>
    public class PDMeanHandshake : IStrategy
    {
        private StrategyInfo info;

        public StrategyInfo GetStrategyInfo()
        {
            return info;
        }

        public List<OptionInfo> GetOptions()
        {
            return null;
        }

        public void SetOptions(List<OptionInfo> options)
        {
            ;
        }

        public void SetPointScheme(PointScheme pointScheme)
        {
            Reward = pointScheme.Reward;
            Punishment = pointScheme.Punishment;
            Temptation = pointScheme.Temptation;
            Payoff = pointScheme.Payoff;
        }

        private int Reward = 0;
        private int Punishment = 0;
        private int Temptation = 0;
        private int Payoff = 0;

        public PDMeanHandshake()
        {
            info = new StrategyInfo();
            info.StrategyName = "Handshake";
            info.StrategyType = StrategyTypes.Mean;
            info.Description = string.Empty;
            info.Description += "Defects on the first move." + Environment.NewLine;
            info.Description += "Cooperates on the second move." + Environment.NewLine;
            info.Description += "If the opponent behaves the same way --> Cooperate thereafter." + Environment.NewLine;
            info.Description += "Otherwise --> Defect thereater.";
            info.HasOptions = false;
        }

        public bool PlayCard(
                                int turn,
                                List<Card> movesHistorySelf,
                                List<Card> movesHistoryOpponent,
                                List<int> pointHistorySelf,
                                List<int> pointHistoryOpponent, out Card card)
        {
            bool ret = true;
            card = Card.Defect;

            try
            {
                if (turn == 2)
                {
                    card = Card.Cooperate;
                }
                else if (turn >= 3)
                {
                    if (movesHistoryOpponent[0] == movesHistorySelf[0] &&
                       movesHistoryOpponent[1] == movesHistorySelf[1])
                    {
                        card = Card.Cooperate;
                    }
                    else
                    {
                        card = Card.Defect;
                    }
                }
            }
            catch
            {
                ret = false;
            }

            return ret;
        }

        public bool Flush()
        {
            return true;
        }
    }
}
