﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StrategyCommon;
using PrisonersDilemma.StrategyCommon;
using static PrisonersDilemma.StrategyCommon.CommonTypes;

namespace PrisonersDilemmaStrategies
{
    /// <summary>
    /// Starts with D,C,C.
    /// Defect --> If the opponent has cooperated in the second and third move.
    /// Play Tit for Tat --> Otherwise.
    /// </summary>
    public class PDMeanProber : IStrategy
    {
        private StrategyInfo info;

        public StrategyInfo GetStrategyInfo()
        {
            return info;
        }

        public List<OptionInfo> GetOptions()
        {
            return null;
        }

        public void SetOptions(List<OptionInfo> options)
        {
            ;
        }

        public void SetPointScheme(PointScheme pointScheme)
        {
            Reward = pointScheme.Reward;
            Punishment = pointScheme.Punishment;
            Temptation = pointScheme.Temptation;
            Payoff = pointScheme.Payoff;
        }

        private int Reward = 0;
        private int Punishment = 0;
        private int Temptation = 0;
        private int Payoff = 0;

        public PDMeanProber()
        {
            info = new StrategyInfo();
            info.StrategyName = "Prober";
            info.StrategyType = StrategyTypes.Mean;
            info.Description = string.Empty;
            info.Description += "Starts with D, C, C." + Environment.NewLine;
            info.Description += "Defect --> If the opponent has cooperated in the second and third move." + Environment.NewLine;
            info.Description += "Play Tit for Tat --> Otherwise.";
            info.HasOptions = false;
        }

        public bool PlayCard(
                                int turn,
                                List<Card> movesHistorySelf,
                                List<Card> movesHistoryOpponent,
                                List<int> pointHistorySelf,
                                List<int> pointHistoryOpponent, out Card card)
        {
            bool ret = true;
            card = Card.Defect;

            try
            {
                if (turn == 1 || turn == 2)
                {
                    card = Card.Defect;
                }
                else if (turn == 3)
                {
                    card = Card.Cooperate;
                }
                else if (turn >= 4)
                {
                    if (movesHistoryOpponent[1] == Card.Cooperate && movesHistoryOpponent[2] == Card.Cooperate)
                    {
                        card = Card.Defect;
                    }
                    else
                    {
                        card = movesHistoryOpponent[movesHistoryOpponent.Count - 1];
                    }
                }
            }
            catch
            {
                ret = false;
            }

            return ret;
        }

        public bool Flush()
        {
            return true;
        }
    }
}
