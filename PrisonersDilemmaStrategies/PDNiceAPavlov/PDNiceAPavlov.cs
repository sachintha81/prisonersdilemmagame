﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StrategyCommon;
using PrisonersDilemma.StrategyCommon;
using static PrisonersDilemma.StrategyCommon.CommonTypes;

namespace PrisonersDilemmaStrategies
{
    /// <summary>
    /// Plays TFT in the first six moves and identifies the opponent by means of a rule-based mechanism. 
    /// The strategies of the opponent are categorized into four groups: cooperative, AllD, STFT, and Random. 
    ///     #   If the opponent does not start defecting, it is identified to be cooperative 
    ///         and then APavlov will behave as TFT. 
    ///     #   If the opponent defects more than four times in six consecutive moves, it is identified as an AllD type
    ///         and then APavlov will always defect. 
    ///     #   If the opponent just defects three times in six moves, it is identified as STFT type 
    ///         and then APavlov will adopt TFTT in order to recover mutual cooperation. 
    ///     #   Any strategy that does not belong to the former three categories will be identified as a random type. 
    ///         In this situation, APavlov will always defect. 
    /// 
    /// In order to deal with the situations in which the opponents may change their actions, 
    /// the average payoff is computed every six rounds. 
    /// If it is lower than a threshold, the process of opponent identification may restart.
    /// </summary>
    public class PDNiceAPavlov : IStrategy
    {
        private StrategyInfo info;

        public StrategyInfo GetStrategyInfo()
        {
            return info;
        }

        public List<OptionInfo> GetOptions()
        {
            List<OptionInfo> options = new List<OptionInfo>();
            options.Add(new OptionInfo(1, "Threshold", "double", threshold, min, max));
            return options;
        }

        public void SetOptions(List<OptionInfo> options)
        {
            threshold = Convert.ToDouble(options[0].OptValue);
        }

        public void SetPointScheme(PointScheme pointScheme)
        {
            Reward = pointScheme.Reward;
            Punishment = pointScheme.Punishment;
            Temptation = pointScheme.Temptation;
            Payoff = pointScheme.Payoff;
        }

        private int Reward = 0;
        private int Punishment = 0;
        private int Temptation = 0;
        private int Payoff = 0;

        private enum TypesOfOpponents
        {
            Cooperative, AllD, STFT, Random
        }

        private double threshold = 0;
        private double min = 0;
        private double max = Convert.ToDouble(MinMax.NO_MAX);
        private static TypesOfOpponents opponentType;

        public PDNiceAPavlov()
        {
            info = new StrategyInfo();
            info.StrategyName = "APavlov";
            info.StrategyType = StrategyTypes.Nice;
            info.Description = string.Empty;
            info.Description += "Plays TFT in the first six moves and identifies the opponent by means of a rule-based mechanism." + Environment.NewLine;
            info.Description += "The strategies of the opponent are categorized into four groups: cooperative, AllD, STFT, and Random." + Environment.NewLine;
            info.Description += "     #   If the opponent does not start defecting, it is identified to be cooperative and then APavlov will behave as Tit-For-Tat." + Environment.NewLine;
            info.Description += "     #   If the opponent defects more than four times in six consecutive moves, it is identified as an AllD type and then APavlov will always defect." + Environment.NewLine;
            info.Description += "     #   If the opponent just defects three times in six moves, it is identified as STFT type and then APavlov will adopt TFTT in order to recover mutual cooperation." + Environment.NewLine;
            info.Description += "     #   Any strategy that does not belong to the former three categories will be identified as a random type. In this situation, APavlov will always defect." + Environment.NewLine;
            info.Description += Environment.NewLine;
            info.Description += "In order to deal with the situations in which the opponents may change their actions, the average payoff is computed every six rounds." + Environment.NewLine;
            info.Description += "If it is lower than a threshold, the process of opponent identification may restart." + Environment.NewLine;
            info.Description += "The Threshold must be greater than 0.";
            info.HasOptions = true;
            threshold = Reward;
        }

        public bool PlayCard(
                                int turn,
                                List<Card> movesHistorySelf,
                                List<Card> movesHistoryOpponent,
                                List<int> pointHistorySelf,
                                List<int> pointHistoryOpponent, out Card card)
        {
            bool ret = true;
            card = Card.Cooperate;

            try
            {
                if (turn > 1)
                {
                    int lastIndex = movesHistorySelf.Count - 1;
                    if (turn == 2 || turn == 3 || turn == 4 || turn == 5 || turn == 6)
                    {
                        card = movesHistoryOpponent[lastIndex];
                    }
                    else if (turn == 7)
                    {
                        opponentType = FindOpponentType(movesHistoryOpponent);

                        card = DecideCard(opponentType, movesHistoryOpponent);
                    }
                    else if (turn > 7)
                    {
                        if (turn % 6 == 0)
                        {
                            double avgPayoff = CalculateAveragePayoff(pointHistorySelf);
                            if (avgPayoff < threshold)
                            {
                                opponentType = FindOpponentType(movesHistoryOpponent);
                            }
                        }
                        card = DecideCard(opponentType, movesHistoryOpponent);
                    }
                }
            }
            catch
            {
                ret = false;
            }

            return ret;
        }

        private Card DecideCard(TypesOfOpponents oppType, List<Card> movesHistoryOpponent)
        {
            Card card = Card.None;
            int lastIndex = movesHistoryOpponent.Count - 1;

            switch (oppType)
            {
                case TypesOfOpponents.Cooperative:
                    // Play Tit for Tat.
                    card = movesHistoryOpponent[lastIndex];
                    break;

                case TypesOfOpponents.AllD:
                    // Defect at all times.
                    card = Card.Defect;
                    break;

                case TypesOfOpponents.STFT:
                    // Play Tit for Two Tats.
                    if (movesHistoryOpponent[lastIndex] == Card.Defect && movesHistoryOpponent[lastIndex - 1] == Card.Defect)
                    {
                        card = Card.Defect;
                    }
                    else
                    {
                        card = Card.Cooperate;
                    }
                    break;

                case TypesOfOpponents.Random:
                    // Defect at all times.
                    card = Card.Defect;
                    break;
            }

            return card;
        }

        private double CalculateAveragePayoff(List<int> pointsListSelf)
        {
            double avgPayoff = 0;

            int total = pointsListSelf.Sum();
            avgPayoff = (double)total / pointsListSelf.Count;

            return avgPayoff;
        }

        private TypesOfOpponents FindOpponentType(List<Card> opponentMoves)
        {
            TypesOfOpponents oppType;

            List<Card> lastSixMoves = opponentMoves.GetRange(opponentMoves.Count - 6, 6);

            if (!lastSixMoves.Contains(Card.Defect))
            {
                oppType = TypesOfOpponents.Cooperative;
            }
            else if (lastSixMoves.Count(x => x.Equals(Card.Defect)) > 4)
            {
                oppType = TypesOfOpponents.AllD;
            }
            else if (lastSixMoves.Count(x => x.Equals(Card.Defect)) <= 3)
            {
                oppType = TypesOfOpponents.STFT;
            }
            else
            {
                oppType = TypesOfOpponents.Random;
            }

            return oppType;
        }

        public bool Flush()
        {
            threshold = 0;
            return true;
        }
    }
}