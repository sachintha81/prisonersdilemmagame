﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StrategyCommon;
using PrisonersDilemma.StrategyCommon;
using static PrisonersDilemma.StrategyCommon.CommonTypes;

namespace PrisonersDilemmaStrategies
{
    /// <summary>
    /// Starts with C,C,C,C,C,D,D,D,D,D.
    /// Then takes choices which have given the best average score re-calculated after every move.
    /// </summary>
    public class PDNiceAdaptive : IStrategy
    {
        private StrategyInfo info;

        public StrategyInfo GetStrategyInfo()
        {
            return info;
        }

        public List<OptionInfo> GetOptions()
        {
            return null;
        }

        public void SetOptions(List<OptionInfo> options)
        {
            ;
        }

        public void SetPointScheme(PointScheme pointScheme)
        {
            Reward = pointScheme.Reward;
            Punishment = pointScheme.Punishment;
            Temptation = pointScheme.Temptation;
            Payoff = pointScheme.Payoff;
        }

        private int Reward = 0;
        private int Punishment = 0;
        private int Temptation = 0;
        private int Payoff = 0;

        public PDNiceAdaptive()
        {
            info = new StrategyInfo();
            info.StrategyName = "Adaptive";
            info.StrategyType = StrategyTypes.Nice;
            info.Description = string.Empty;
            info.Description += "Starts with C,C,C,C,C,D,D,D,D,D." + Environment.NewLine;
            info.Description += "Then takes choices which have given the best average score re-calculated after every move.";
            info.HasOptions = false;
        }

        public bool PlayCard(
                                int turn,
                                List<Card> movesHistorySelf,
                                List<Card> movesHistoryOpponent,
                                List<int> pointHistorySelf,
                                List<int> pointHistoryOpponent, out Card card)
        {
            bool ret = true;
            card = Card.Cooperate;

            try
            {
                if (turn == 1 || turn == 2 || turn == 3 || turn == 4 || turn == 5)
                {
                    card = Card.Cooperate;
                }
                else if (turn == 6 || turn == 7 || turn == 8 || turn == 9 || turn == 10)
                {
                    card = Card.Defect;
                }
                else if (turn >= 11)
                {
                    card = FindBestMove(movesHistorySelf, pointHistorySelf);
                }
            }
            catch
            {
                ret = false;
            }

            return ret;
        }

        private Card FindBestMove(List<Card> movesHistorySelf, List<int> pointsHistorySelf)
        {
            Card card = Card.None;

            int cPoints = 0;
            int dPoints = 0;
            int numOfC = 0;
            int numOfD = 0;

            for (int i = 0; i < movesHistorySelf.Count; i++)
            {
                if (movesHistorySelf[i] == Card.Cooperate)
                {
                    cPoints += pointsHistorySelf[i];
                    numOfC++;
                }
                if (movesHistorySelf[i] == Card.Defect)
                {
                    dPoints += pointsHistorySelf[i];
                    numOfD++;
                }
            }

            double avgC = (double)cPoints / numOfC;
            double avgD = (double)dPoints / numOfD;

            if (avgC >= avgD)
            {
                card = Card.Cooperate;
            }
            else
            {
                card = Card.Defect;
            }

            return card;
        }

        public bool Flush()
        {
            return true;
        }
    }
}
