﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StrategyCommon;
using PrisonersDilemma.StrategyCommon;
using static PrisonersDilemma.StrategyCommon.CommonTypes;

namespace PrisonersDilemmaStrategies
{
    /// <summary>
    /// Plays C and D in the first and second moves. 
    /// If the opponent has played the same moves, plays TFT. 
    /// Otherwise, plays AllD.
    /// </summary>
    public class PDNiceCollectiveStrategy : IStrategy
    {
        private StrategyInfo info;

        public StrategyInfo GetStrategyInfo()
        {
            return info;
        }

        public List<OptionInfo> GetOptions()
        {
            return null;
        }

        public void SetOptions(List<OptionInfo> options)
        {
            ;
        }

        public void SetPointScheme(PointScheme pointScheme)
        {
            Reward = pointScheme.Reward;
            Punishment = pointScheme.Punishment;
            Temptation = pointScheme.Temptation;
            Payoff = pointScheme.Payoff;
        }

        private bool isSimilar = false;

        private int Reward = 0;
        private int Punishment = 0;
        private int Temptation = 0;
        private int Payoff = 0;

        public PDNiceCollectiveStrategy()
        {
            info = new StrategyInfo();
            info.StrategyName = "Collective Strategy";
            info.StrategyType = StrategyTypes.Nice;
            info.Description = string.Empty;
            info.Description += "Plays C and D in the first and second moves." + Environment.NewLine;
            info.Description += "If the opponent has played the same moves, plays TFT." + Environment.NewLine;
            info.Description += "Otherwise, plays AllD.";
            info.HasOptions = false;
        }

        public bool PlayCard(
                                int turn,
                                List<Card> movesHistorySelf,
                                List<Card> movesHistoryOpponent,
                                List<int> pointHistorySelf,
                                List<int> pointHistoryOpponent, out Card card)
        {
            bool ret = true;
            card = Card.Defect;

            try
            {
                if (turn == 1)
                {
                    card = Card.Defect;
                }
                else if (turn == 2)
                {
                    card = Card.Defect;
                }
                else if (turn == 3)
                {
                    if (movesHistorySelf[0] == movesHistoryOpponent[0] &&
                        movesHistorySelf[1] == movesHistoryOpponent[1])
                    {
                        isSimilar = true;
                    }
                    else
                    {
                        isSimilar = false;
                    }
                }

                if (turn >= 3)
                {
                    if (isSimilar)
                    {
                        card = movesHistoryOpponent[movesHistoryOpponent.Count - 1];
                    }
                    else
                    {
                        card = Card.Defect;
                    }
                }
            }
            catch
            {
                ret = false;
            }

            return ret;
        }

        public bool Flush()
        {
            isSimilar = false;
            return true;
        }
    }
}
