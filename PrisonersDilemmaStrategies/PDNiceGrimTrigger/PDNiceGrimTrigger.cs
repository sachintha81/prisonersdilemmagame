﻿using System;
using System.Collections.Generic;
using StrategyCommon;
using PrisonersDilemma.StrategyCommon;
using static PrisonersDilemma.StrategyCommon.CommonTypes;

namespace PrisonersDilemmaStrategies
{
    /// <summary>
    /// Cooperates at first.
    /// If opponent defects, defects thereafter.
    /// </summary>
    public class PDNiceGrimTrigger : IStrategy
    {
        private StrategyInfo info;

        public StrategyInfo GetStrategyInfo()
        {
            return info;
        }

        public List<OptionInfo> GetOptions()
        {
            return null;
        }

        public void SetOptions(List<OptionInfo> options)
        {
            ;
        }

        public void SetPointScheme(PointScheme pointScheme)
        {
            Reward = pointScheme.Reward;
            Punishment = pointScheme.Punishment;
            Temptation = pointScheme.Temptation;
            Payoff = pointScheme.Payoff;
        }

        private bool opponentDefected = false;

        private int Reward = 0;
        private int Punishment = 0;
        private int Temptation = 0;
        private int Payoff = 0;

        public PDNiceGrimTrigger()
        {
            info = new StrategyInfo();
            info.StrategyName = "Grim Trigger";
            info.StrategyType = StrategyTypes.Nice;
            info.Description = string.Empty;
            info.Description += "Cooperates at first." + Environment.NewLine;
            info.Description += "If opponent defects, defects thereafter.";
            info.HasOptions = false;
        }

        public bool PlayCard(
                                int turn,
                                List<Card> movesHistorySelf,
                                List<Card> movesHistoryOpponent,
                                List<int> pointHistorySelf,
                                List<int> pointHistoryOpponent, out Card card)
        {
            bool ret = true;
            card = Card.Cooperate;

            try
            {
                int lastIndex = movesHistoryOpponent.Count - 1;

                if (turn > 1)
                {
                    if (movesHistoryOpponent[lastIndex] == Card.Defect)
                    {
                        opponentDefected = true;
                    }
                    if (opponentDefected)
                    {
                        card = Card.Defect;
                    }
                }
            }
            catch
            {
                ret = false;
            }

            return ret;
        }

        public bool Flush()
        {
            opponentDefected = false;
            return true;
        }
    }
}

