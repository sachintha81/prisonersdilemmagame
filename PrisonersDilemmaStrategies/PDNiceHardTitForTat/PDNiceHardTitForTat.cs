﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StrategyCommon;
using PrisonersDilemma.StrategyCommon;
using static PrisonersDilemma.StrategyCommon.CommonTypes;

namespace PrisonersDilemmaStrategies
{
    /// <summary>
    /// Cooperates at first.
    /// Thereafter
    ///     Defect    --> If the opponent defected in any of the last 3 moves.
    ///     Cooperate --> Otherwise.
    /// </summary>
    public class PDNiceHardTitForTat : IStrategy
    {
        private StrategyInfo info;

        public StrategyInfo GetStrategyInfo()
        {
            return info;
        }

        public List<OptionInfo> GetOptions()
        {
            return null;
        }

        public void SetOptions(List<OptionInfo> options)
        {
            ;
        }

        public void SetPointScheme(PointScheme pointScheme)
        {
            Reward = pointScheme.Reward;
            Punishment = pointScheme.Punishment;
            Temptation = pointScheme.Temptation;
            Payoff = pointScheme.Payoff;
        }

        private int Reward = 0;
        private int Punishment = 0;
        private int Temptation = 0;
        private int Payoff = 0;

        public PDNiceHardTitForTat()
        {
            info = new StrategyInfo();
            info.StrategyName = "Hard Tit for Tat";
            info.StrategyType = StrategyTypes.Nice;
            info.Description = string.Empty;
            info.Description += "Cooperates at first." + Environment.NewLine;
            info.Description += "Thereafter:" + Environment.NewLine;
            info.Description += "     Defect    --> If the opponent defected in any of the last 3 moves." + Environment.NewLine;
            info.Description += "     Cooperate --> Otherwise.";
            info.HasOptions = false;
        }

        public bool PlayCard(
                                int turn,
                                List<Card> movesHistorySelf,
                                List<Card> movesHistoryOpponent,
                                List<int> pointHistorySelf,
                                List<int> pointHistoryOpponent, out Card card)
        {
            bool ret = true;
            card = Card.Cooperate;

            try
            {
                int lastIndex = movesHistoryOpponent.Count - 1;

                if (turn == 2 && movesHistoryOpponent[lastIndex] == Card.Defect)
                {
                    card = Card.Defect;
                }
                else if (turn == 3)
                {
                    if (movesHistoryOpponent[lastIndex] == Card.Defect || movesHistoryOpponent[lastIndex - 1] == Card.Defect)
                    {
                        card = Card.Defect;
                    }
                }
                else if (turn >= 4)
                {
                    if (movesHistoryOpponent[lastIndex] == Card.Defect || movesHistoryOpponent[lastIndex - 1] == Card.Defect || movesHistoryOpponent[lastIndex - 2] == Card.Defect)
                    {
                        card = Card.Defect;
                    }
                }
            }
            catch
            {
                ret = false;
            }

            return ret;
        }

        public bool Flush()
        {
            return true;
        }
    }
}