﻿using System;
using System.Collections.Generic;
using StrategyCommon;
using PrisonersDilemma.StrategyCommon;
using static PrisonersDilemma.StrategyCommon.CommonTypes;

namespace PrisonersDilemmaStrategies
{
    /// <summary>
    /// Cooperates at first.
    /// If Reward or Temptation received in the last round, repeat your last move.
    /// Otherwise, play the opposite of your last move.
    /// </summary>
    public class PDNicePavlov : IStrategy
    {
        private StrategyInfo info;

        public StrategyInfo GetStrategyInfo()
        {
            return info;
        }

        public List<OptionInfo> GetOptions()
        {
            return null;
        }

        public void SetOptions(List<OptionInfo> options)
        {
            ;
        }

        public void SetPointScheme(PointScheme pointScheme)
        {
            Reward = pointScheme.Reward;
            Punishment = pointScheme.Punishment;
            Temptation = pointScheme.Temptation;
            Payoff = pointScheme.Payoff;
        }

        private int Reward = 0;
        private int Punishment = 0;
        private int Temptation = 0;
        private int Payoff = 0;

        public PDNicePavlov()
        {
            info = new StrategyInfo();
            info.StrategyName = "Pavlov";
            info.StrategyType = StrategyTypes.Nice;
            info.Description = string.Empty;
            info.Description += "Cooperates at first." + Environment.NewLine;
            info.Description += "If Reward or Temptation received in the last round, repeat your last move." + Environment.NewLine;
            info.Description += "Otherwise, play the opposite of your last move.";
            info.HasOptions = false;
        }

        public bool PlayCard(
                                int turn,
                                List<Card> movesHistorySelf,
                                List<Card> movesHistoryOpponent,
                                List<int> pointHistorySelf,
                                List<int> pointHistoryOpponent, out Card card)
        {
            bool ret = true;
            card = Card.Cooperate;

            try
            {
                int lastIndex = movesHistoryOpponent.Count - 1;
                Card myLastMove = Card.None;
                int myLastPoints = 0;

                if (turn > 1)
                {
                    myLastMove = movesHistorySelf[lastIndex];
                    myLastPoints = pointHistorySelf[lastIndex];

                    if (myLastPoints == Reward || myLastPoints == Temptation)
                    {
                        card = myLastMove;
                    }
                    else {
                        if (myLastMove == Card.Cooperate)
                        {
                            card = Card.Defect;
                        }
                        else {
                            card = Card.Cooperate;
                        }
                    }
                }
            }
            catch
            {
                ret = false;
            }

            return ret;
        }

        public bool Flush()
        {
            return true;
        }
    }
}

