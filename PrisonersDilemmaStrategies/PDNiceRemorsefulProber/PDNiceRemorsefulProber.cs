﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using StrategyCommon;
using PrisonersDilemma.StrategyCommon;
using static PrisonersDilemma.StrategyCommon.CommonTypes;

namespace PrisonersDilemmaStrategies
{
    /// <summary>
    /// Cooperates at first.
    /// Repeats the opponent's move thereafter.
    /// Occationally defects with a small probability q.
    /// Generally q must be lower than 0.2
    /// If there is a series of mutual defections, breaks it with a cooperate.
    /// </summary>
    public class PDNiceRemorsefulProber : IStrategy
    {
        private StrategyInfo info;

        public StrategyInfo GetStrategyInfo()
        {
            return info;
        }

        public List<OptionInfo> GetOptions()
        {
            List<OptionInfo> options = new List<OptionInfo>();
            options.Add(new OptionInfo(1, "Defect Probability", "double", probability, min, max));
            return options;
        }

        public void SetOptions(List<OptionInfo> options)
        {
            probability = Convert.ToDouble(options[0].OptValue);
        }

        public void SetPointScheme(PointScheme pointScheme)
        {
            Reward = pointScheme.Reward;
            Punishment = pointScheme.Punishment;
            Temptation = pointScheme.Temptation;
            Payoff = pointScheme.Payoff;
        }

        private double probability = 0;
        private double min = 0;
        private double max = 1;

        private int Reward = 0;
        private int Punishment = 0;
        private int Temptation = 0;
        private int Payoff = 0;

        public PDNiceRemorsefulProber()
        {
            info = new StrategyInfo();
            info.StrategyName = "Remorseful Prober";
            info.StrategyType = StrategyTypes.Nice;
            info.Description = string.Empty;
            info.Description += "Cooperates at first." + Environment.NewLine;
            info.Description += "Repeats the opponent's move thereafter." + Environment.NewLine;
            info.Description += "Occationally defects with a small probability q." + Environment.NewLine;
            info.Description += "Generally q must be lower than 0.2" + Environment.NewLine;
            info.Description += "If there is a series of mutual defections, breaks it with a cooperate.";
            info.HasOptions = true;

            probability = 0.1;
        }

        public bool PlayCard(
                                int turn,
                                List<Card> movesHistorySelf,
                                List<Card> movesHistoryOpponent,
                                List<int> pointHistorySelf,
                                List<int> pointHistoryOpponent, out Card card)
        {
            bool ret = true;
            card = Card.Cooperate;

            try
            {
                if (turn > 1)
                {
                    int lastIndex = movesHistoryOpponent.Count - 1;
                    card = movesHistoryOpponent[lastIndex];
                    if (card == Card.Defect)
                    {
                        card = Card.Cooperate;
                    }
                    else {
                        int[] results = new int[100];
                        byte roll = GetRandomNumber((byte)results.Length);
                        double prob = (Convert.ToDouble(roll)) / 100;
                        if (prob <= probability)
                        {
                            card = Card.Defect;
                        }
                    }
                }
            }
            catch
            {
                ret = false;
            }

            return ret;
        }

        public bool Flush()
        {
            return true;
        }

        public static byte GetRandomNumber(byte numberSides)
        {
            RandomNumberGenerator rng = new RNGCryptoServiceProvider();
            if (numberSides <= 0)
            {
                throw new ArgumentOutOfRangeException("numberSides");
            }

            // Create a byte array to hold the random value.
            byte[] randomNumber = new byte[1];
            do
            {
                // Fill the array with a random value.
                rng.GetBytes(randomNumber);
            }
            while (!IsFairRoll(randomNumber[0], numberSides));
            // Return the random number mod the number
            // of sides.  The possible values are zero-
            // based.
            return (byte)(randomNumber[0] % numberSides);
        }

        private static bool IsFairRoll(byte roll, byte numSides)
        {
            // There are MaxValue / numSides full sets of numbers that can come up
            // in a single byte.  For instance, if we have a 6 sided die, there are
            // 42 full sets of 1-6 that come up.  The 43rd set is incomplete.
            int fullSetsOfValues = Byte.MaxValue / numSides;

            // If the roll is within this range of fair values, then we let it continue.
            // In the 6 sided die case, a roll between 0 and 251 is allowed.  (We use
            // < rather than <= since the = portion allows through an extra 0 value).
            // 252 through 255 would provide an extra 0, 1, 2, 3 so they are not fair
            // to use.
            return roll < numSides * fullSetsOfValues;
        }
    }
}
