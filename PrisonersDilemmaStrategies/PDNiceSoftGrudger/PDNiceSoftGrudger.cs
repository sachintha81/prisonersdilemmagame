﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StrategyCommon;
using PrisonersDilemma.StrategyCommon;
using static PrisonersDilemma.StrategyCommon.CommonTypes;

namespace PrisonersDilemmaStrategies
{
    /// <summary>
    /// Cooperates at first.
    /// If opponent defects, punishes with D,D,D,D.
    /// Continues with C thereafter unless the opponent defects again.
    /// </summary>
    public class PDNiceSoftGrudger : IStrategy
    {
        private StrategyInfo info;

        public StrategyInfo GetStrategyInfo()
        {
            return info;
        }

        public List<OptionInfo> GetOptions()
        {
            return null;
        }

        public void SetOptions(List<OptionInfo> options)
        {
            ;
        }

        public void SetPointScheme(PointScheme pointScheme)
        {
            Reward = pointScheme.Reward;
            Punishment = pointScheme.Punishment;
            Temptation = pointScheme.Temptation;
            Payoff = pointScheme.Payoff;
        }

        private bool opponentDefected = false;
        private int punishCount = 0;

        private int Reward = 0;
        private int Punishment = 0;
        private int Temptation = 0;
        private int Payoff = 0;

        public PDNiceSoftGrudger()
        {
            info = new StrategyInfo();
            info.StrategyName = "Soft Grudger";
            info.StrategyType = StrategyTypes.Nice;
            info.Description = string.Empty;
            info.Description += "Cooperates at first." + Environment.NewLine;
            info.Description += "If opponent defects, punishes with D,D,D,D." + Environment.NewLine;
            info.Description += "Continues with C thereafter unless the opponent defects again.";
            info.HasOptions = false;
        }

        public bool PlayCard(
                                int turn,
                                List<Card> movesHistorySelf,
                                List<Card> movesHistoryOpponent,
                                List<int> pointHistorySelf,
                                List<int> pointHistoryOpponent, out Card card)
        {
            bool ret = true;
            card = Card.Cooperate;

            try
            {
                int lastIndex = movesHistoryOpponent.Count - 1;

                if (turn > 1)
                {
                    if (movesHistoryOpponent[lastIndex] == Card.Defect)
                    {
                        opponentDefected = true;
                    }
                    if (opponentDefected)
                    {
                        card = Card.Defect;
                        punishCount++;
                        if (punishCount == 4)
                        {
                            punishCount = 0;
                            opponentDefected = false;
                        }
                    }
                }
            }
            catch
            {
                ret = false;
            }

            return ret;
        }

        public bool Flush()
        {
            opponentDefected = false;
            punishCount = 0;
            return true;
        }
    }
}
