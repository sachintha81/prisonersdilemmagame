﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StrategyCommon;
using PrisonersDilemma.StrategyCommon;
using static PrisonersDilemma.StrategyCommon.CommonTypes;

namespace PrisonersDilemmaStrategies
{
    /// <summary>
    /// Cooperates at first.
    /// Thereafter:
    ///     Cooperate --> If No. of C by opponent >= No. of D by opponent
    ///     Defect    --> Otherwise
    /// </summary>
    public class PDNiceSoftMajority : IStrategy
    {
        private StrategyInfo info;

        public StrategyInfo GetStrategyInfo()
        {
            return info;
        }

        public List<OptionInfo> GetOptions()
        {
            return null;
        }

        public void SetOptions(List<OptionInfo> options)
        {
            ;
        }

        public void SetPointScheme(PointScheme pointScheme)
        {
            Reward = pointScheme.Reward;
            Punishment = pointScheme.Punishment;
            Temptation = pointScheme.Temptation;
            Payoff = pointScheme.Payoff;
        }

        private int opponentC = 0;
        private int opponentD = 0;

        private int Reward = 0;
        private int Punishment = 0;
        private int Temptation = 0;
        private int Payoff = 0;

        public PDNiceSoftMajority()
        {
            info = new StrategyInfo();
            info.StrategyName = "Soft Majority";
            info.StrategyType = StrategyTypes.Nice;
            info.Description = string.Empty;
            info.Description += "Cooperates at first." + Environment.NewLine;
            info.Description += "Thereafter:" + Environment.NewLine;
            info.Description += "     Cooperate --> If No. of C by opponent >= No. of D by opponent." + Environment.NewLine;
            info.Description += "     Defect    --> Otherwise";
            info.HasOptions = false;
        }

        public bool PlayCard(
                                int turn,
                                List<Card> movesHistorySelf,
                                List<Card> movesHistoryOpponent,
                                List<int> pointHistorySelf,
                                List<int> pointHistoryOpponent, out Card card)
        {
            bool ret = true;
            card = Card.Cooperate;

            try
            {
                int lastIndex = movesHistoryOpponent.Count - 1;

                if (turn > 1)
                {
                    if (movesHistoryOpponent[lastIndex] == Card.Cooperate)
                    {
                        opponentC++;
                    }
                    else if (movesHistoryOpponent[lastIndex] == Card.Defect)
                    {
                        opponentD++;
                    }

                    if (opponentC >= opponentD)
                    {
                        card = Card.Cooperate;
                    }
                    else
                    {
                        card = Card.Defect;
                    }
                }
            }
            catch
            {
                ret = false;
            }

            return ret;
        }

        public bool Flush()
        {
            opponentC = 0;
            opponentD = 0;
            return true;
        }
    }
}
