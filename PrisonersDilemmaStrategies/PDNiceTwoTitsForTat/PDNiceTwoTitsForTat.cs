﻿using System;
using System.Collections.Generic;
using StrategyCommon;
using PrisonersDilemma.StrategyCommon;
using static PrisonersDilemma.StrategyCommon.CommonTypes;

namespace PrisonersDilemmaStrategies
{
    /// <summary>
    /// Cooperates at first.
    /// Copy opponent's last move.
    /// However, defect twice for opponent's every defect.
    /// </summary>
    public class PDNiceTwoTitsForTat : IStrategy
    {
        private StrategyInfo info;

        public StrategyInfo GetStrategyInfo()
        {
            return info;
        }

        public List<OptionInfo> GetOptions()
        {
            return null;
        }

        public void SetOptions(List<OptionInfo> options)
        {
            ;
        }

        public void SetPointScheme(PointScheme pointScheme)
        {
            Reward = pointScheme.Reward;
            Punishment = pointScheme.Punishment;
            Temptation = pointScheme.Temptation;
            Payoff = pointScheme.Payoff;
        }

        private int opponentDCount = 0;
        private int repliedDCount = 0;

        private int Reward = 0;
        private int Punishment = 0;
        private int Temptation = 0;
        private int Payoff = 0;

        public PDNiceTwoTitsForTat()
        {
            info = new StrategyInfo();
            info.StrategyName = "Two Tits for Tat";
            info.StrategyType = StrategyTypes.Nice;
            info.Description = string.Empty;
            info.Description += "Cooperates at first." + Environment.NewLine;
            info.Description += "Copy opponent's last move." + Environment.NewLine;
            info.Description += "However, defect twice for opponent's every defect.";
            info.HasOptions = false;
        }

        public bool PlayCard(
                                int turn,
                                List<Card> movesHistorySelf,
                                List<Card> movesHistoryOpponent,
                                List<int> pointHistorySelf,
                                List<int> pointHistoryOpponent, out Card card)
        {
            bool ret = true;
            card = Card.Cooperate;

            try
            {
                int lastIndex = movesHistoryOpponent.Count - 1;

                if (turn > 1)
                {
                    if (movesHistoryOpponent[lastIndex] == Card.Defect)
                    {
                        opponentDCount++;
                    }
                    if ((opponentDCount > 0) && (repliedDCount < (opponentDCount * 2)))
                    {
                        card = Card.Defect;
                        repliedDCount++;
                    }
                }
            }
            catch
            {
                ret = false;
            }

            return ret;
        }

        public bool Flush()
        {
            opponentDCount = 0;
            repliedDCount = 0;
            return true;
        }
    }
}

