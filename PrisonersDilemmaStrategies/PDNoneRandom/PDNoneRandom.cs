﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using StrategyCommon;
using PrisonersDilemma.StrategyCommon;
using static PrisonersDilemma.StrategyCommon.CommonTypes;

namespace PrisonersDilemmaStrategies
{
    /// <summary>
    /// Plays a card at random.
    /// </summary>
    public class PDNoneRandom : IStrategy
    {
        private StrategyInfo info;

        public StrategyInfo GetStrategyInfo()
        {
            return info;
        }

        public List<OptionInfo> GetOptions()
        {
            return null;
        }

        public void SetOptions(List<OptionInfo> options)
        {
            ;
        }

        public void SetPointScheme(PointScheme pointScheme)
        {
            Reward = pointScheme.Reward;
            Punishment = pointScheme.Punishment;
            Temptation = pointScheme.Temptation;
            Payoff = pointScheme.Payoff;
        }

        private int Reward = 0;
        private int Punishment = 0;
        private int Temptation = 0;
        private int Payoff = 0;

        public PDNoneRandom()
        {
            info = new StrategyInfo();
            info.StrategyName = "Random";
            info.StrategyType = StrategyTypes.None;
            info.Description = string.Empty;
            info.Description += "Plays a card at random.";
            info.HasOptions = false;
        }

        public bool PlayCard(
                                int turn,
                                List<Card> movesHistorySelf,
                                List<Card> movesHistoryOpponent,
                                List<int> pointHistorySelf,
                                List<int> pointHistoryOpponent, out Card card)
        {
            bool ret = true;
            card = Card.Cooperate;

            try
            {
                int[] results = new int[2];
                byte roll = GetRandomNumber((byte)results.Length);

                if (roll == 1)
                {
                    card = Card.Defect;
                }
            }
            catch
            {
                ret = false;
            }

            return ret;
        }

        public bool Flush()
        {
            return true;
        }

        public static byte GetRandomNumber(byte numberSides)
        {
            RandomNumberGenerator rng = new RNGCryptoServiceProvider();
            if (numberSides <= 0)
            {
                throw new ArgumentOutOfRangeException("numberSides");
            }

            // Create a byte array to hold the random value.
            byte[] randomNumber = new byte[1];
            do
            {
                // Fill the array with a random value.
                rng.GetBytes(randomNumber);
            }
            while (!IsFairRoll(randomNumber[0], numberSides));
            // Return the random number mod the number
            // of sides.  The possible values are zero-
            // based.
            return (byte)(randomNumber[0] % numberSides);
        }

        private static bool IsFairRoll(byte roll, byte numSides)
        {
            // There are MaxValue / numSides full sets of numbers that can come up
            // in a single byte.  For instance, if we have a 6 sided die, there are
            // 42 full sets of 1-6 that come up.  The 43rd set is incomplete.
            int fullSetsOfValues = Byte.MaxValue / numSides;

            // If the roll is within this range of fair values, then we let it continue.
            // In the 6 sided die case, a roll between 0 and 251 is allowed.  (We use
            // < rather than <= since the = portion allows through an extra 0 value).
            // 252 through 255 would provide an extra 0, 1, 2, 3 so they are not fair
            // to use.
            return roll < numSides * fullSetsOfValues;
        }
    }
}