# README #

### This is a desktop application to run the Iterated Prisoner's Dilemma. ###

* The application is divided into 3 main parts:
	1. Run one strategy against another one strategy.
	2. Run one strategy against multiple selected strategies.
	3. Run a number of selected strategies against each other.
* Part 1. has been completed.
* Parts 2 and 3 are yet to be completed.
* The individual strategies are coded in separate DLLs, which the main program reads off of a folder.
	* At the moment, the DLLs must be placed in the same directory where the executable is located. 
	* Please open the 'PrisonersDilemmaStrategies' solution in VS and build. The resulting libraries will be copied to the [PrisonersDilemmaStrategies/Libraries] directory. Copy them to the directory which the application executable is located.
	* I will change this later so that a user can specify the path.

### Technical Stuff ###
* This application is developed on Microsoft .NET framework 4.6 and C# language.
* For the GUIs, I've used WPF and the MVVM pattern. Almost zero code behind.

### What is Iterated Prisoner's Dilemma? ###
* Prisoner's Dilemma is a simple game that often appear in Game Theory.
	* More Info: https://en.wikipedia.org/wiki/Prisoner%27s_dilemma
* Iterated Prisoner's Dilemma is a modified version of the above where the game is palyed between two individuals multiple number of times, and the two individuals do not know when the last move will be.
* This program focuses on the evolutionary biology aspect of the game, which shows that cooperation among individuals can come about over evolutionary time. 
* Inspired by the original research on the subject done by Dr. Robert Axelrod, and use of the results to explain the evolutionary biology implications by Dr. Richard Dawkins.

### System Requirements ###
* Windows 7 or later. (Hasn't been tested on other platforms.)
* Microsoft .NET framework 4.0 or later.

### Install Instructions ###

* TBD

### Contact Me ###

* sachintha81 [at] gmail.com
